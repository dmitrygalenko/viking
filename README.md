A little 2d game where you play as a lone viking, defending from waves of enemies with different weapons.

Playable demo: https://dmitry-galenko.github.io/untitled-viking-game/

You can also find MacOS and Windows builds in the "builds" directory.