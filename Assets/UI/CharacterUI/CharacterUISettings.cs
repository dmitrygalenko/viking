using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "CharacterUISettings", menuName = "Settings/CharacterUISettings")]
    public class CharacterUISettings : ScriptableObject
    {
        [System.Serializable]
        public class UISettings
        {
            public GroupID groupID;
            public Color m_healthBarColour;
            public Color m_shieldBarColour;
            public Color m_helmetBarColour;
        }

        [SerializeField] private List<UISettings> m_uiSettings;

        public UISettings GetSettings(GroupID groupID)
        {
            return m_uiSettings.Find(item => item.groupID == groupID);
        }
    }
}
