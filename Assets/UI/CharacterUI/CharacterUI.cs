using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CharacterUI : MonoBehaviour
    {
        [SerializeField] private GameObject m_ammoBar;
        [SerializeField] private GameObject m_noAmmo;
        [SerializeField] private List<GameObject> m_ammo;

        public enum UIHudType
        {
            Health,
            Shield,
            Helmet,
        }

        [System.Serializable]
        private class UIBarInfo
        {
            public UIHudType type;
            public ProgressBar m_healthBar;
        }

        [SerializeField] private List<UIBarInfo> m_bars;

        public void Init(CharacterUISettings.UISettings settings)
        {
            GetProgressBar(UIHudType.Health)?.SetFillColour(settings.m_healthBarColour);
            GetProgressBar(UIHudType.Shield)?.SetFillColour(settings.m_shieldBarColour);
            GetProgressBar(UIHudType.Helmet)?.SetFillColour(settings.m_helmetBarColour);
        }

        public UIHudType GetUIType(ItemType type)
        {
            switch (type)
            {
                case ItemType.Body:
                    return UIHudType.Health;
                case ItemType.Shield:
                    return UIHudType.Shield;
                case ItemType.Helmet:
                    return UIHudType.Helmet;
            }
            return UIHudType.Health;
        }

        private ProgressBar GetProgressBar(UIHudType type)
        {
            return m_bars.Find(item => item.type == type).m_healthBar;

        }

        public void ShowBar(UIHudType type, bool value, bool animate)
        {
            if (value || !animate)
                GetProgressBar(type)?.gameObject.SetActive(value);
            if (animate)
                GetProgressBar(type).ChangeVisibility(value);
        }

        public void Reset()
        {
            for (int i = 0; i < m_bars.Count; ++i)
            {
                m_bars[i].m_healthBar.Reset();
            }
        }

        public void OnDead()
        {
            for (int i = 0; i < m_bars.Count; ++i)
            {
                m_bars[i].m_healthBar.ChangeVisibility(false);
            }
        }

        public void ShowAmmo(bool show)
        {
            m_ammoBar.gameObject.SetActive(show);
        }

        public void SetAmmo(int value)
        {
            for (int i = 0; i < m_ammo.Count; ++i)
            {
                m_ammo[i].SetActive(value >= i + 1);
            }

            var noAmmoLeft = value <= 0;
            m_noAmmo.SetActive(noAmmoLeft);
        }

        public void SetRatio(UIHudType type, float ratio, float diff = 0f)
        {
            GetProgressBar(type)?.SetRatio(ratio, diff);
        }

        public void UpdateUI(float timeDelta)
        {
            for (int i = 0; i < m_bars.Count; ++i)
            {
                m_bars[i].m_healthBar.UpdateBar(timeDelta);
            }
        }
    }
}
