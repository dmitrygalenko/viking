using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainMenu
{
    public class SettingsScreen : MonoBehaviour
    {
        [SerializeField] private Animation m_animation;
        [SerializeField] private RectTransform m_fullView;
        [SerializeField] private RectTransform m_shortView;
        private string m_showClip = "SettingsAppear";
        private string m_hideClip = "SettingsHide";
        private bool m_shown;

        public bool IsShown()
        {
            return m_shown;
        }

        public void Show(bool withInfo)
        {
            m_fullView.gameObject.SetActive(withInfo);
            m_shortView.gameObject.SetActive(!withInfo);
            m_animation.Play(m_showClip);
            m_shown = true;
        }

        public void OnClosePressed()
        {
            Hide();
        }

        public void Hide()
        {
            m_shown = false;
            m_animation.Play(m_hideClip);
        }
    }
}
