using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Gameplay
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private UnityEngine.UI.Image m_fill;
        [SerializeField] private Animation m_animation;
        private string m_damageAnimClip = "ProgressBarAnim";
        private string m_appearAnimClip = "Added";
        private string m_brokenAnimClip = "Broken";
        private string m_resetHealthAnimClip = "ResetHealth";
        [Header("on hit anims")]
        [SerializeField] private Color m_diffColour;
        [SerializeField] private UnityEngine.UI.Image m_diff;
        private float m_visualRatio;
        [SerializeField] private float m_initialHold;
        private float m_hold = 0f;
        [SerializeField] private float m_fallRatio;
        [SerializeField] private TextMeshProUGUI m_diffText;
        private float m_oldRatio;

        public void SetFillColour(Color colour)
        {
            m_fill.color = colour;
            m_diffText.faceColor = colour;
            m_diff.color = m_diffColour;
        }

        public void SetRatio(float ratio, float diff = 0f)
        {
            m_fill.fillAmount = ratio;
            if (diff != 0f)
            {
                m_diffText.text = diff.ToString();
                m_animation.Play(m_damageAnimClip);

                m_visualRatio = m_oldRatio;
                m_diff.fillAmount = m_visualRatio;
                m_hold = m_initialHold;
            }
            m_oldRatio = ratio;
        }

        private void UpdateDiff(float timeDelta)
        {
            if (m_visualRatio > m_oldRatio)
            {
                if (m_hold > 0f)
                {
                    m_hold -= timeDelta;
                }
                else
                {
                    m_visualRatio = Mathf.Max(m_oldRatio, m_visualRatio - timeDelta * m_fallRatio);
                    m_diff.fillAmount = m_visualRatio;
                }
            }
        }

        public void UpdateBar(float timeDelta)
        {
            UpdateDiff(timeDelta);
        }

        public void Reset()
        {
            m_animation.Play(m_resetHealthAnimClip);
        }

        public void ChangeVisibility(bool show)
        {
            var clipId = show ? m_appearAnimClip : m_brokenAnimClip;
            m_animation.Play(clipId);
        }
    }
}
