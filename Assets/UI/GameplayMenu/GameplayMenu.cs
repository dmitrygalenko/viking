using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GameplayMenu
{
    public class GameplayMenu : MonoBehaviour
    {
        public enum MenuType
        {
            Won,
            Lost,
            Pause,
        }

        private MenuType m_type;

        [SerializeField] private TextMeshProUGUI m_text;
        [SerializeField] private TextMeshProUGUI m_leftButtonText;
        [SerializeField] private TextMeshProUGUI m_rightButtonText;
        [SerializeField] private Animation m_animation;
        [SerializeField] private MainMenu.HintsDisplay m_hints;
        private string m_showClip = "ResultAppear";
        private string m_hideClip = "MenuDisappear";
        private Gameplay.GameplayLogic m_gameplayLogic;
        private bool m_shown;
        private bool m_canHide;

        public void Init(Gameplay.GameplayLogic gameplayLogic)
        {
            m_gameplayLogic = gameplayLogic;
            m_shown = false;
            m_canHide = false;
        }

        public bool IsShown()
        {
            return m_shown;
        }

        public bool CanHide()
        {
            return m_canHide;
        }

        public void Show(MenuType type)
        {
            m_type = type;
            m_canHide = false;
            m_hints.Init();

            switch (m_type)
            {
                case MenuType.Won:
                    m_text.text = "VICTORY";
                    m_leftButtonText.text = "MENU";
                    m_rightButtonText.text = "RESTART";
                    break;
                case MenuType.Lost:
                    m_text.text = "YOU LOST";
                    m_leftButtonText.text = "MENU";
                    m_rightButtonText.text = "TRY AGAIN";
                    break;
                case MenuType.Pause:
                    m_text.text = "PAUSE";
                    m_leftButtonText.text = "MENU";
                    m_rightButtonText.text = "RETURN";
                    m_canHide = true;
                    break;
            }

            m_animation.Play(m_showClip);
            m_shown = true;
        }

        public void OnLeftButton()
        {
            switch (m_type)
            {
                case MenuType.Won:
                case MenuType.Lost:
                case MenuType.Pause:
                    m_gameplayLogic.Close();
                    break;
            }
        }

        public void OnRightButton()
        {
            switch (m_type)
            {
                case MenuType.Won:
                case MenuType.Lost:
                    m_gameplayLogic.Restart();
                    break;
                case MenuType.Pause:
                    HideMenu();
                    m_gameplayLogic.SetActive(true);
                    break;
            }
        }

        public void RequestHide()
        {
            if (m_canHide)
            {
                HideMenu();
            }
        }

        public void OnSettingsPressed()
        {
            m_gameplayLogic.ShowSettings(true);
        }

        private void HideMenu()
        {
            m_shown = false;
            m_animation.Play(m_hideClip);
        }
    }
}
