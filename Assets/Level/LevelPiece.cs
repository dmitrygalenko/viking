using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class LevelPiece : MonoBehaviour
    {
        [SerializeField] private Decor[] m_decor;

        private void OnValidate()
        {
            m_decor = gameObject.GetComponentsInChildren<Decor>();
        }

        public void Init(Services services)
        {
            for (int i = 0; i < m_decor.Length; ++i)
            {
                m_decor[i].Init(services);
            }
        }
    }
}
