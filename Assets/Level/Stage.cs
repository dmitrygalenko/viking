using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Stage : MonoBehaviour
    {
        [SerializeField] private StageSettings m_settings;
        [SerializeField] private string m_ID;
        [SerializeField] private Collider2D m_collider;
        private Services m_services;

        public void Init(Services services)
        {
            m_services = services;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other == null)
                return;

            var playerPositionTrigger = other.GetComponent<PositionTrigger>();
            if (playerPositionTrigger == null)
                return;

            if (playerPositionTrigger.IsPlayable())
            {
                EnableTriggers(false);
                StartStage();
            }
        }

        public void EnableTriggers(bool value)
        {
            m_collider.enabled = value;
        }

        public string GetID()
        {
            return m_ID;
        }

        private void StartStage()
        {
            m_services.levelController.StartStage(this);
        }

        public StageSettings GetSettings()
        {
            return m_settings;
        }
    }
}
