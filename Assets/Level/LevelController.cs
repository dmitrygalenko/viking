using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class LevelController : MonoBehaviour
    {
        private Character m_mainCharacter;
        [SerializeField] private LevelContainer m_levelContainer;
        private Stage m_currentStage;
        private int m_currentWaveID;
        private List<Character> m_enemiesActive;
        private int m_completeStages = 0;
        private class SpawnRequest
        {
            public float time;
            public StageSettings.CharacterSpawnInfo info;
        }
        private List<SpawnRequest> m_spawnQueue;
        private Services m_services;

        public void Init(CharacterDescription startingCharacterDescription, ColourDescription startingColourDescription, Services services)
        {
            m_services = services;
            m_enemiesActive = new List<Character>();
            m_spawnQueue = new List<SpawnRequest>();

            m_levelContainer.Init(m_services);
            services.charactersController.SetHorizontalMovementLimit(m_levelContainer.GetXBounds());
            services.charactersController.SetVerticalMovementLimit(m_levelContainer.GetYBounds());

            var position = m_levelContainer.GetStartingSpawnPoint();
            m_mainCharacter = m_services.charactersController.AddCharacter(startingCharacterDescription, startingColourDescription, GroupID.Player, position.transform.position, 1f, isMain: true);
        }

        public Character GetMainCharacter()
        {
            return m_mainCharacter;
        }

        public void StartStage(Stage stage)
        {
            m_currentStage = stage;
            m_services.cameraController.SetObject(stage.transform);
            m_currentWaveID = 0;
            LoadWave();

            var limit = GetCurrentStageHorizontalLimits();
            m_services.charactersController.SetHorizontalMovementLimit(limit);
        }

        private Vector2 GetCurrentStageHorizontalLimits()
        {
            return new Vector2(
                m_currentStage.transform.position.x - m_services.settings.GetStageLimitDistance(),
                m_currentStage.transform.position.x + m_services.settings.GetStageLimitDistance());
        }

        private void LoadWave()
        {
            var stageSettings = m_currentStage.GetSettings();
            if (stageSettings.GetWavesCount() <= m_currentWaveID)
            {
                EndStage();
                return;
            }

            var waveData = stageSettings.GetWave(m_currentWaveID);
            SpawnCharacters(waveData);
        }

        private void SpawnCharacters(StageSettings.Wave wave)
        {
            for (int i = 0; i < wave.characters.Length; ++i)
            {
                var data = wave.characters[i];
                var frame = data.timeFrame;
                if (frame.y < frame.x)
                    frame.y = frame.x;
                var timeDelta = UnityEngine.Random.Range(frame.x, frame.y);
                m_spawnQueue.Add(new SpawnRequest()
                {
                    info = data,
                    time = timeDelta,
                });
            }
        }

        private void Spawn(StageSettings.CharacterSpawnInfo info)
        {
            var bounds = m_levelContainer.GetYBounds();
            var Y = UnityEngine.Random.Range(bounds.x, bounds.y);
            var direction = info.side == StageSettings.Side.Left ? 1f : -1f;
            var X = m_currentStage.transform.position.x + m_services.settings.GetCharacterSpawnDistance() * direction * -1f;
            var position = new Vector3(X, Y, 0f);
            var character = m_services.charactersController.AddCharacter(info.character.GetSpecific(), colourInfo: null, info.groupID, position, direction);
            m_enemiesActive.Add(character);
        }

        private void ProcessSpawnQueue(float timeDelta)
        {
            for (int i = 0; i < m_spawnQueue.Count; ++i)
            {
                if (m_spawnQueue[i].time <= 0)
                {
                    Spawn(m_spawnQueue[i].info);
                }
            }

            m_spawnQueue.RemoveAll(item => item.time <= 0f);

            for (int i = 0; i < m_spawnQueue.Count; ++i)
            {
                m_spawnQueue[i].time -= timeDelta;
            }
        }

        private void CheckCharacters(float timeDelta)
        {
            m_enemiesActive.RemoveAll(item => !item.IsAlive());

            if (m_enemiesActive.Count == 0 && m_spawnQueue.Count == 0)
            {
                EndWave();
            }
        }

        public void UpdateController(float timeDelta)
        {
            if (m_currentStage != null)
            {
                CheckCharacters(timeDelta);
                ProcessSpawnQueue(timeDelta);
            }

            m_levelContainer.UpdateController(timeDelta, m_services.cameraController.GetCameraX(), 0.4f);
        }

        private void EndWave()
        {
            ++m_currentWaveID;
            LoadWave();
        }

        public void EndStage()
        {
            m_currentStage = null;
            m_services.cameraController.SetObject(m_mainCharacter.transform);
            m_services.charactersController.SetHorizontalMovementLimit(m_levelContainer.GetXBounds());
            ++m_completeStages;
            CheckLevelCompletion();
        }

        private void CheckLevelCompletion()
        {
            if (m_completeStages >= m_levelContainer.GetStagesCount())
            {
                m_services.gameplayLogic.OnLevelComplete();
            }
        }

        public List<Character> GetAllCharacters()
        {
            return m_services.charactersController.GetAllCharacters();
        }

        public Vector2 GetHorizontalMovementLimit()
        {
            if (m_currentStage != null)
                return GetCurrentStageHorizontalLimits();

            return m_levelContainer.GetXBounds();
        }
    }
}
