using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "StageDescription", menuName = "Settings/StageDescription")]
    public class StageSettings : ScriptableObject
    {
        public enum Side
        {
            Left,
            Right,
        }

        [System.Serializable]
        public class CharacterSpawnInfo
        {
            public CharacterDescriptionRange character;
            public GroupID groupID = GroupID.Enemy;
            public Side side = Side.Right;
            public Vector2 timeFrame = new Vector2(0f, 1f);
        }

        [System.Serializable]
        public class Wave
        {
            public CharacterSpawnInfo[] characters;
        }

        [SerializeField] private List<Wave> m_waves;

        public int GetWavesCount()
        {
            return m_waves.Count;
        }

        public Wave GetWave(int waveID)
        {
            return m_waves[waveID];
        }
    }
}
