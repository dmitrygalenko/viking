using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "StartingCharacterStats", menuName = "Settings/StartingCharacterStats")]
    public class StartingCharacterStats : ScriptableObject
    {
        [SerializeField] private CharacterDescriptionRange m_stats;

        public CharacterDescription GetDescription()
        {
            return m_stats.GetSpecific();
        }
    }
}
