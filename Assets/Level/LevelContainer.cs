using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class LevelContainer : MonoBehaviour
    {
        [SerializeField] private SpawnPoint m_startingSpawnPoint;
        [SerializeField] private Stage[] m_stages;
        private Services m_services;
        [SerializeField] private Transform m_leftBound;
        [SerializeField] private Transform m_rightBound;
        [SerializeField] private Transform m_topBound;
        [SerializeField] private Transform m_bottomBound;
        private Vector2 m_horizontalBounds;
        private Vector2 m_verticalBounds;
        [SerializeField] private Decor[] m_decor;
        [SerializeField] private List<Pickup> m_pickupList;
        [SerializeField] private LevelPiece[] m_pieces;
        [SerializeField] private Transform m_back;
        private float m_baseBackX;

        public void Init(Services services)
        {
            m_services = services;

            for (int i = 0; i < m_stages.Length; ++i)
            {
                m_stages[i].Init(services);
            }

            for (int i = 0; i < m_decor.Length; ++i)
            {
                m_decor[i].Init(services);
            }

            for (int i = 0; i < m_pieces.Length; ++i)
            {
                m_pieces[i].Init(services);
            }

            for (int i = 0; i < m_pickupList.Count; ++i)
            {
                m_pickupList[i].Init(services);
            }

            m_horizontalBounds = new Vector2(m_leftBound.position.x, m_rightBound.position.x);
            m_verticalBounds = new Vector2(m_bottomBound.position.y, m_topBound.position.y);

            m_baseBackX = m_back.transform.position.x;
        }

        public SpawnPoint GetStartingSpawnPoint()
        {
            return m_startingSpawnPoint;
        }

        public Vector2 GetYBounds()
        {
            return m_verticalBounds;
        }

        public Vector2 GetXBounds()
        {
            return m_horizontalBounds;
        }

        public int GetStagesCount()
        {
            return m_stages.Length;
        }

        public void UpdateController(float timeDelta, float cameraX, float parallaxAmount)
        {
            var position = m_back.transform.position;
            position.x = m_baseBackX + cameraX * parallaxAmount;
            m_back.transform.position = position;
        }
    }
}
