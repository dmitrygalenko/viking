using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Helpers
{
    public class WeightedItem<T>
    {
        public float weight;
        public T item;
    }
}
