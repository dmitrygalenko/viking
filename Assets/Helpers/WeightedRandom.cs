using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Helpers
{
    public static class WeightedRandom<T, P> where P : WeightedItem<T>
    {
        public static T GetWeightedItem(List<P> items)
        {
            if (items.Count == 1)
                return items[0].item;

            var totalWeight = 0f;
            for (int i = 0; i < items.Count; ++i)
            {
                totalWeight += items[i].weight;
            }

            var chosenRandom = UnityEngine.Random.Range(0, totalWeight);

            var currentTotalWeight = 0f;
            for (int i = 0; i < items.Count; ++i)
            {
                if (items[i].weight <= 0)
                    continue;

                currentTotalWeight += items[i].weight;
                if (currentTotalWeight >= chosenRandom)
                {
                    return items[i].item;
                }
            }

            return default(T);
        }
    }
}
