using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CharacterDescription
    {
        public ItemID weapon;
        public ItemID shield;
        public ItemID helmet;
        public float health;
        public float scale;
    }
}
