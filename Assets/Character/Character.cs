using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Character : MonoBehaviour, IVisuallySortable, IPoolableItem
    {
        [SerializeField] private Animator m_animator;
        [SerializeField] private ItemCustomisation m_characterCustomisation;
        [SerializeField] private ItemSettingsList m_characterSettingsList;
        [SerializeField] private ItemCustomisation m_characterHairCustomisation;
        [SerializeField] private ItemSettingsList m_characterHairSettingsList;
        [SerializeField] private ItemCustomisation m_teamCustomisation;
        [SerializeField] private TeamSettingsList m_teamSettingsList;
        [SerializeField] private GameObject m_characterContainer;
        [SerializeField] private CharacterPartWeapon m_weapon;
        [SerializeField] private CharacterPart m_shield;
        [SerializeField] private CharacterPart m_helmet;
        [SerializeField] private CharacterPart m_body;
        [SerializeField] private CharacterUI m_characterUI;
        [SerializeField] private CharacterUISettings m_characterUISettings;
        [SerializeField] private SortingLayerUpdater m_sortingUpdater;
        [SerializeField] private MapObjectScaler m_mapObjectScaler;
        [Header("AI")]
        [SerializeField] private AI.AICharacter m_AI;
        [Header("move")]
        [SerializeField] private float m_moveSpeed;
        private float m_destroyTimer;
        private bool m_shallDestroy;
        private Pickup m_currentPickup;

        private float m_cooldownTime;
        private Vector2 m_verticalMovementLimit;
        private Vector2 m_horizontalMovementLimit;
        [Header("blood")]
        [SerializeField] private ParticleSystem m_bloodFX;
        [SerializeField] private Transform m_groundVFXArea;
        [SerializeField] private GameObject[] m_blockFX;
        [SerializeField] private GameObject m_effectsContainer;
        private float m_blockVFXTime;
        [SerializeField] private float m_blockVFXTimeMax;
        [SerializeField] private GameObject m_footstepObject;
        private float m_currentAnimatorSpeed = 1f;
        private Vector3 m_pushbackVector;

        private class AnimatorParams
        {
            public int walkspeedAnimParam = Animator.StringToHash("walk_speed");
            public int isAttackingHTriggerParam = Animator.StringToHash("is_attacking_H_trigger");
            public int isAttackingLTriggerParam = Animator.StringToHash("is_attacking_L_trigger");
            public int isRollingTriggerParam = Animator.StringToHash("roll_trigger");
            public int isDefendingParam = Animator.StringToHash("is_defending");
            public int isHitTriggerParam = Animator.StringToHash("is_hit_trigger");
            public int isDeadParam = Animator.StringToHash("is_dead");
            public int ragesTriggerParam = Animator.StringToHash("rages_trigger");
            public int pickupTriggerParam = Animator.StringToHash("pick_up_trigger");
            public int on_hit_blockedParam = Animator.StringToHash("on_hit_blocked");
        }

        private AnimatorParams m_animatorParameters = new AnimatorParams();

        private Vector3 m_currentSpeed = Vector3.zero;
        private bool m_isDefending = false;
        private bool m_isAttacking = false;

        private GroupID m_groupID;
        private int m_playerId;
        private bool m_playerControlled;
        private float m_maxHealth;
        private float m_health;
        private Vector3 m_aimingVector;
        private Services m_services;
        private ColourDescription m_colourDescription;

        public void Init(int playerID, GroupID groupID, CharacterDescription info, ColourDescription colourDescription, Services services)
        {
            m_services = services;
            m_destroyTimer = 0f;
            m_shallDestroy = false;
            m_groupID = groupID;
            m_playerId = playerID;
            m_maxHealth = info.health;
            m_health = m_maxHealth;
            m_aimingVector = new Vector3(1f, 0f, 0f);
            m_pushbackVector = Vector3.zero;

            m_mapObjectScaler.Init(m_services);
            if (m_characterContainer.transform.localScale.x < 0)
                m_aimingVector.x = -1f;

            var randomColours = true;
            if (colourDescription != null)
            {
                m_colourDescription = colourDescription;
                randomColours = false;
            }
            else
            {
                m_colourDescription = new ColourDescription()
                {
                    bodyColour = m_characterSettingsList.GetRandomId(),
                    hairColour = m_characterHairSettingsList.GetRandomId(),
                    weaponColour = -1,
                    shieldColour = -1,
                    helmetColour = -1,
                };
            }

            var settings = m_characterSettingsList.GetSettings(m_colourDescription.bodyColour);
            m_characterCustomisation.SetColour(settings.mainColour);

            var hairSettings = m_characterHairSettingsList.GetSettings(m_colourDescription.hairColour);
            m_characterHairCustomisation.SetColour(hairSettings.mainColour);

            var teamColourSettings = m_teamSettingsList.GetSettings(m_groupID);
            m_teamCustomisation.SetColour(teamColourSettings.mainColour);

            m_weapon.Init(m_playerId, this, m_services);
            m_shield.Init(m_playerId, this, m_services);
            m_helmet.Init(m_playerId, this, m_services);
            m_body.Init(m_playerId, this, m_services);

            InitUI();

            SetItem(ItemType.Weapon, info.weapon, m_colourDescription.weaponColour, animateUI: false);
            SetItem(ItemType.Shield, info.shield, m_colourDescription.shieldColour, animateUI: false);
            SetItem(ItemType.Helmet, info.helmet, m_colourDescription.helmetColour, animateUI: false);
            SetItem(ItemType.Body, ItemID.None, -1, animateUI: false);

            m_characterUI.Reset();

            if (randomColours)
            {
                m_colourDescription.weaponColour = m_weapon.GetCurrentItem().GetColourInd();
                m_colourDescription.shieldColour = m_shield.GetCurrentItem().GetColourInd();
                m_colourDescription.helmetColour = m_helmet.GetCurrentItem().GetColourInd();
            }

            if (m_sortingUpdater != null && services != null)
                m_sortingUpdater.SetSortableObject(this, services.settings);

            m_currentPickup = null;

            SetScale(info.scale);

            ChangeAnimatorSpeed(1f);
            m_cooldownTime = 0f;
            m_isAttacking = false;
            m_isDefending = false;
        }

        private void InitUI()
        {
            m_characterUI.gameObject.SetActive(true);
            var settings = m_characterUISettings.GetSettings(m_groupID);
            m_characterUI.Init(settings);
            m_characterUI.SetRatio(CharacterUI.UIHudType.Health, GetHealthRatio());
        }

        public void SetDirection(float x)
        {
            if (IsOnCooldown() || IsDefending())
                return;

            if (Mathf.Abs(x) < 0.001f)
                return;

            var transform = m_characterContainer.transform;
            var scale = transform.localScale;
            scale.x = x > 0f ? 1f : -1f;
            transform.localScale = scale;
        }

        public float GetDirection()
        {
            return m_characterContainer.transform.localScale.x;
        }

        private void SetScale(float scale)
        {
            m_mapObjectScaler.SetBaseScale(scale);
        }

        private void Walk(Vector3 speed)
        {
            var animWalkSpeed = Mathf.Abs(speed.x) > 0f || Mathf.Abs(speed.y) > 0f ? 1f : 0f;
            m_animator.SetFloat(m_animatorParameters.walkspeedAnimParam, animWalkSpeed);
        }

        public void AttackH()
        {
            if (IsOnCooldown() || IsDefending())
                return;

            ChangeAnimatorSpeed(m_weapon.GetCurrentWeapon().GetAttackSpeed());

            SetCooldown(GameplaySettings.CooldownID.HeavyAttack);
            Stop();

            m_isAttacking = true;
            m_animator.SetTrigger(m_animatorParameters.isAttackingHTriggerParam);
        }

        public void AttackL()
        {
            if (IsOnCooldown() || IsDefending())
                return;

            ChangeAnimatorSpeed(m_weapon.GetCurrentWeapon().GetAttackSpeed());

            SetCooldown(GameplaySettings.CooldownID.LightAttack);
            Stop();

            m_isAttacking = true;
            m_animator.SetTrigger(m_animatorParameters.isAttackingLTriggerParam);
        }

        public void Defend(bool active)
        {
            if (m_isDefending != active)
            {
                if (!m_isDefending && IsOnCooldown())
                    return;

                if (!IsOnCooldown())
                    SetCooldown(GameplaySettings.CooldownID.Defend);

                Stop();

                m_isDefending = active;
                m_animator.SetBool(m_animatorParameters.isDefendingParam, m_isDefending);
                m_shield.EnableColliders(active);
            }
        }

        private void Hit()
        {
            if (!IsDefending())
                SetCooldown(GameplaySettings.CooldownID.GetHit);
            else
                SetCooldown(GameplaySettings.CooldownID.BlockHit);
            m_animator.SetTrigger(m_animatorParameters.isHitTriggerParam);
        }

        public bool IsDefending()
        {
            return m_isDefending;
        }

        public bool HasRangedWeapon()
        {
            return m_weapon.GetCurrentWeapon().IsRanged();
        }

        public float GetAttackRange()
        {
            if (HasRangedWeapon() && HasAmmo())
                return 7f;
            else
                return 4f;
        }

        public bool HasAmmo()
        {
            return m_weapon.GetCurrentWeapon().GetAmmoLeft() > 0;
        }

        public void Die()
        {
            Defend(false);
            m_animator.SetBool(m_animatorParameters.isDeadParam, true);
            m_characterUI.OnDead();
            m_body.EnableColliders(false);
            m_shield.EnableColliders(false);
            m_weapon.EnableColliders(false);
            m_helmet.EnableColliders(false);
            ShowBloodVFX(false);
            ShowBlockVFX(false);

            m_destroyTimer = m_services.settings.GetDespawnTime();
        }

        public void Reset()
        {
            m_animator.SetBool(m_animatorParameters.isDeadParam, false);
            m_destroyTimer = 0f;
            m_shallDestroy = false;
            gameObject.SetActive(false);
        }

        public void Cry()
        {
            if (IsOnCooldown() || IsDefending())
                return;

            m_services.cameraController.AddShake(CameraController.ShakeIntensity.Small);
            SetCooldown(GameplaySettings.CooldownID.Cry);
            Stop();

            m_animator.SetTrigger(m_animatorParameters.ragesTriggerParam);
        }

        public void Roll()
        {
            if (IsOnCooldown())
                return;

            SetCooldown(GameplaySettings.CooldownID.EvasiveRoll);

            m_animator.SetTrigger(m_animatorParameters.isRollingTriggerParam);
        }

        public void Action()
        {
            m_animator.SetTrigger(m_animatorParameters.pickupTriggerParam);
            if (m_currentPickup != null)
            {
                var reward = m_currentPickup.GetReward();
                ApplyReward(reward);
            }
        }

        private void ApplyReward(Reward reward)
        {
            if (reward.itemId != ItemID.None)
            {
                SetItem(reward.itemType, reward.itemId, reward.colourInd, animateUI: true);
            }
        }

        public void Stop()
        {
            m_currentSpeed.x = 0f;
            m_currentSpeed.y = 0f;
            m_animator.SetFloat(m_animatorParameters.walkspeedAnimParam, 0f);
        }

        public void Move(float x, float y)
        {
            if (IsOnCooldown() || IsDefending())
            {
                m_currentSpeed.x = 0f;
                m_currentSpeed.y = 0f;
                return;
            }

            m_currentSpeed.x = x;
            m_currentSpeed.y = y * m_services.settings.GetYToXRatio();
            m_currentSpeed *= m_moveSpeed;
        }

        private void ProcessMove(float timeDelta)
        {
            var potentialMove = m_currentSpeed * timeDelta;

            var currentPosition = transform.position;
            if (potentialMove.x < 0f && currentPosition.x + potentialMove.x < m_horizontalMovementLimit.x)
            {
                potentialMove.x = m_horizontalMovementLimit.x - currentPosition.x;
            }
            else if (potentialMove.x > 0f && currentPosition.x + potentialMove.x > m_horizontalMovementLimit.y)
            {
                potentialMove.x = m_horizontalMovementLimit.y - currentPosition.x;
            }

            if (potentialMove.y < 0f && currentPosition.y + potentialMove.y < m_verticalMovementLimit.x)
            {
                potentialMove.y = m_verticalMovementLimit.x - currentPosition.y;
            }
            else if (potentialMove.y > 0f && currentPosition.y + potentialMove.y > m_verticalMovementLimit.y)
            {
                potentialMove.y = m_verticalMovementLimit.y - currentPosition.y;
            }

            if (m_services.charactersController.IsPathBlocked(potentialMove + currentPosition, GetPlayerID()))
            {
                return;
            }

            transform.Translate(potentialMove);
            SetDirection(potentialMove.x);
            Walk(potentialMove);

            UpdateTargeting(potentialMove.x, potentialMove.y);
        }

        public Vector3 GetCurrentSpeed()
        {
            return m_currentSpeed;
        }

        private void UpdateTargeting(float x, float y)
        {
            if (Mathf.Abs(x) > 0.1f || Mathf.Abs(y) > 0.1f)
            {
                y *= m_services.settings.GetYToXRatio();

                var minxAbsX = m_services.settings.GetMinVerticalTargeting();
                if (Mathf.Abs(x) < minxAbsX)
                {
                    var sign = Mathf.Sign(x);
                    if (x == 0f)
                    {
                        sign = Mathf.Sign(m_aimingVector.x);
                    }
                    m_aimingVector.x = minxAbsX * sign;
                }
                else
                    m_aimingVector.x = x;
                m_aimingVector.y = y;
                m_aimingVector.Normalize();
            }
        }

        public void OnHitAnother(Character otherCharacter, ItemType otherItemType, float damage)
        {
            m_services.cameraController.AddShake(CameraController.ShakeIntensity.Small);
            m_isAttacking = false;
            var weapon = m_weapon.GetCurrentWeapon();
            weapon.EndAttack();
            if (otherItemType == ItemType.Shield || otherItemType == ItemType.Helmet)
            {
                OnHitBlocked();
            }
            else
            {
                m_weapon.GetCurrentItem().Tint(0.1f);
            }

            var direction = transform.position.x - otherCharacter.transform.position.x;
            Pushback(direction, isAttcker: true);
        }

        private void OnHitBlocked()
        {
            SetCooldown(GameplaySettings.CooldownID.OnAttackBlocked);
            m_animator.SetTrigger(m_animatorParameters.on_hit_blockedParam);
            ChangeAnimatorSpeed(1f);
        }

        public void OnGetHit(int otherPlayerId, Item otherItem, Item itemHit, float damage)
        {
            var weapon = m_weapon.GetCurrentWeapon();
            weapon.EndAttack();
            ChangeAnimatorSpeed(1f);
            m_isAttacking = false;
            Hit();
            Stop();

            if (damage > 0f)
            {
                SetVFXDirection((transform.position.x - otherItem.transform.position.x) * -1f * m_characterContainer.transform.localScale.x);
                ShowBloodVFX();
                TakeDamage(damage);
            }
            else
            {
                ShowBlockVFX();
            }

            var direction = itemHit.GetCharacter().transform.position.x - otherItem.GetCharacter().transform.position.x;
            Pushback(direction, isAttcker: false);
        }

        private void Pushback(float direction, bool isAttcker)
        {
            var distance = Mathf.Sign(direction) * m_services.settings.GetPushbackDistance(isAttcker);
            m_pushbackVector.x = distance;
            transform.Translate(m_pushbackVector);
        }

        public bool IsAttacking()
        {
            return m_isAttacking;
        }

        private void SetRandomItem(ItemType itemType, bool animateUI)
        {
            switch (itemType)
            {
                case ItemType.Helmet:
                    m_helmet.SetRandomItem();
                    break;
                case ItemType.Weapon:
                    m_weapon.SetRandomItem();
                    break;
                case ItemType.Shield:
                    m_shield.SetRandomItem();
                    break;
            }

            InitItemUI(itemType, animateUI);
        }

        private void SetItem(ItemType itemType, ItemID itemId, int colourInd, bool animateUI)
        {
            switch (itemType)
            {
                case ItemType.Helmet:
                    m_helmet.SetItem(itemId, colourInd);
                    m_helmet.EnableColliders(true);
                    break;
                case ItemType.Weapon:
                    m_weapon.SetItem(itemId, colourInd);
                    m_weapon.EnableColliders(false);
                    break;
                case ItemType.Shield:
                    m_shield.SetItem(itemId, colourInd);
                    m_shield.EnableColliders(false);
                    break;
                case ItemType.Body:
                    m_body.SetItem(itemId, colourInd);
                    m_body.EnableColliders(true);
                    break;
            }

            InitItemUI(itemType, animateUI);
        }

        public void UpdateItemHealth(Item item, float diff)
        {
            if (!item.IsWorking())
            {
                SetItem(item.GetItemType(), ItemID.None, -1, animateUI: true);
            }
            else
            {
                var itemUIType = m_characterUI.GetUIType(item.GetItemType());
                m_characterUI.SetRatio(itemUIType, item.GetHealthRatio(), diff);
            }
        }

        private void InitItemUI(ItemType itemType, bool animate)
        {
            switch (itemType)
            {
                case ItemType.Shield:
                    var haveShield = !m_shield.IsEmpty();
                    m_characterUI.ShowBar(CharacterUI.UIHudType.Shield, haveShield, animate);
                    if (haveShield)
                        m_characterUI.SetRatio(CharacterUI.UIHudType.Shield, m_shield.GetHealthRatio());
                    break;
                case ItemType.Weapon:
                    var weapon = m_weapon.GetCurrentWeapon();
                    m_characterUI.ShowAmmo(weapon.IsRanged());
                    m_characterUI.SetAmmo(weapon.GetAmmoLeft());
                    break;
                case ItemType.Helmet:
                    var haveHelmet = !m_helmet.IsEmpty();
                    m_characterUI.ShowBar(CharacterUI.UIHudType.Helmet, haveHelmet, animate);
                    if (haveHelmet)
                        m_characterUI.SetRatio(CharacterUI.UIHudType.Helmet, m_helmet.GetHealthRatio());
                    break;
            }
        }

        private void TakeDamage(float amount)
        {
            if (!IsAlive())
                return;

            m_health = Mathf.Max(0f, m_health - amount);
            if (m_health <= 0f)
            {
                Die();
            }
            else
            {
                m_characterUI.SetRatio(CharacterUI.UIHudType.Health, GetHealthRatio(), amount);
            }
        }

        public bool IsAlive()
        {
            return m_health > 0f;
        }

        private float GetHealthRatio()
        {
            return m_health / m_maxHealth;
        }

        public void OnAttackActiveFramesStart()
        {
            var weapon = m_weapon.GetCurrentWeapon();
            weapon.StartAttack();
        }

        public void OnAttackActiveFramesEnd()
        {
            var weapon = m_weapon.GetCurrentWeapon();
            weapon.EndAttack();
            m_isAttacking = false;
            ChangeAnimatorSpeed(1f);
        }

        public void OnShoot()
        {
            var weapon = m_weapon.GetCurrentWeapon();
            if (weapon.IsRanged())
            {
                weapon.Shoot(m_aimingVector, m_services);
                m_characterUI.SetAmmo(weapon.GetAmmoLeft());
            }
        }

        public void OnFootstep()
        {
            m_services.groundFXController.CreateVFX(GroundFXType.Footstep, m_footstepObject.transform.position, m_characterContainer.transform.localScale.x);
            if (transform.localScale.x > 1.2)
                m_services.cameraController.AddShake(CameraController.ShakeIntensity.Small);
        }

        public float GetBaseY()
        {
            return transform.position.y;
        }

        public void OnEnterPickup(Pickup pickup)
        {
            m_currentPickup = pickup;
        }

        public void OnExitPickup(Pickup pickup)
        {
            m_currentPickup = null;
        }

        public int GetPlayerID()
        {
            return m_playerId;
        }

        public GroupID GetGroupID()
        {
            return m_groupID;
        }

        public bool IsPlayerControlled()
        {
            return m_playerControlled;
        }

        public void SetControlledByPlayer(bool isControlled, AI.AISettings AIsettings)
        {
            m_playerControlled = isControlled;

            if (isControlled)
            {
                m_AI.InitDisabled();
            }
            else
            {
                m_AI.Init(this, m_services, AIsettings);
            }
        }

        public void SetVerticalMovementLimit(Vector2 limit)
        {
            m_verticalMovementLimit = limit;
        }

        public void SetHorizontalMovementLimit(Vector2 limit)
        {
            m_horizontalMovementLimit = limit;
        }

        public void UpdateCharacter(float timeDelta)
        {
            if (m_shallDestroy)
                return;

            if (m_destroyTimer > 0f)
            {
                m_destroyTimer -= timeDelta;
                if (m_destroyTimer <= 0f)
                {
                    m_shallDestroy = true;
                }
                return;
            }

            if (!IsOnCooldown() && !IsDefending())
            {
                ProcessMove(timeDelta);
            }

            if (!m_playerControlled && IsAlive())
            {
                m_AI.UpdateAI(timeDelta);
            }

            m_characterUI.UpdateUI(timeDelta);
            UpdateVFX(timeDelta);
            UpdateCooldown(timeDelta);
        }

        public bool IsOnCooldown()
        {
            return m_cooldownTime > 0f;
        }

        private void SetCooldown(GameplaySettings.CooldownID ID)
        {
            m_cooldownTime = m_services.settings.GetCooldownTimer(ID);
        }

        private void SetCooldown(float newValue)
        {
            m_cooldownTime = newValue;
        }

        private void UpdateCooldown(float timeDelta)
        {
            if (m_cooldownTime > 0f)
                m_cooldownTime -= timeDelta;
        }

        private void UpdateVFX(float timeDelta)
        {
            if (m_blockVFXTime > 0f)
            {
                m_blockVFXTime -= timeDelta;
                if (m_blockVFXTime <= 0f)
                    ShowBlockVFX(false);
            }
        }

        private void ShowBloodVFX(bool show = true)
        {
            if (show)
            {
                var emissionShape = m_bloodFX.shape;
                var angles = emissionShape.rotation;
                angles.z = Mathf.Sign(GetDirection()) * 75f;
                emissionShape.rotation = angles;
                m_bloodFX.Play();
                var vfxPos = m_groundVFXArea.transform.position;
                m_services.groundFXController.CreateVFX(GroundFXType.Blood, vfxPos, m_characterContainer.transform.localScale.x);
            }
        }

        private void SetVFXDirection(float x)
        {
            if (Mathf.Abs(x) < 0.1f)
                return;

            var direction = Mathf.Sign(x);
            var scale = m_effectsContainer.transform.localScale;
            scale.x = direction;
            m_effectsContainer.transform.localScale = scale;
        }

        private void ShowBlockVFX(bool show = true)
        {
            var chosenId = -1;
            if (show)
                chosenId = UnityEngine.Random.Range(0, m_blockFX.Length);

            for (int i = 0; i < m_blockFX.Length; ++i)
            {
                m_blockFX[i].SetActive(chosenId == i);
            }

            if (show)
                m_blockVFXTime = m_blockVFXTimeMax;
        }

        public bool GetBattleTarget()
        {
            return m_AI.GetBattleTarget();
        }

        public void SetActive(bool value)
        {
            PauseAnimatorSpeed(pause: !value);
        }

        public float GetRecommendedDistance()
        {
            return m_weapon.GetCurrentWeapon().GetRecommendedDistance();
        }

        public ColourDescription GetColours()
        {
            return m_colourDescription;
        }

        public void InitFromPool()
        {
            gameObject.SetActive(true);
        }

        public bool ShouldRemoveToPool()
        {
            return m_shallDestroy;
        }

        private void ChangeAnimatorSpeed(float speed)
        {
            m_animator.speed = speed;
            m_currentAnimatorSpeed = speed;
        }

        private void PauseAnimatorSpeed(bool pause)
        {
            if (pause)
            {
                m_animator.speed = 0f;
            }
            else
            {
                m_animator.speed = m_currentAnimatorSpeed;
            }
        }
    }
}
