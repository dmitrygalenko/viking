using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class PositionTrigger : MonoBehaviour
    {
        [SerializeField] private Character m_character;

        void OnTriggerEnter2D(Collider2D other)
        {
            var pickup = other.GetComponent<Pickup>();
            if (pickup != null)
            {
                m_character.OnEnterPickup(pickup);
                if (m_character.IsPlayerControlled())
                    pickup.OnEnteredAction();
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            var pickup = other.GetComponent<Pickup>();
            if (pickup != null)
            {
                m_character.OnExitPickup(pickup);
                if (m_character.IsPlayerControlled())
                    pickup.OnLeftAction();
            }
        }

        public bool IsPlayable()
        {
            return m_character.IsPlayerControlled();
        }
    }
}
