using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "ItemSettingsList", menuName = "Settings/ItemSettingsList")]
    public class ItemSettingsList : ScriptableObject
    {
        [System.Serializable]
        public class ItemSettings
        {
            public Color mainColour;
        }

        [SerializeField] private List<ItemSettings> m_settingsList = new List<ItemSettings>();

        public int GetRandomId()
        {
            return UnityEngine.Random.Range(0, m_settingsList.Count);
        }

        public ItemSettings GetSettings(int colourId)
        {
            return m_settingsList[colourId];
        }

        public ItemSettings GetRandomSettings()
        {
            return m_settingsList[UnityEngine.Random.Range(0, m_settingsList.Count)];
        }
    }
}
