using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [System.Serializable]
    public class CharacterDescriptionRange
    {
        [System.Serializable]
        public class WeightedItem : Helpers.WeightedItem<ItemID> { }
        [SerializeField] private WeightedItem[] m_weapons;
        [SerializeField] private WeightedItem[] m_shields;
        [SerializeField] private WeightedItem[] m_helmets;
        [SerializeField] private Vector2 m_healthRange = new Vector2(35f, 50f);
        [SerializeField] private Vector2 m_scaleRange = new Vector2(1f, 1f);

        private ItemID GetItemID(WeightedItem[] m_items)
        {
            var items = new List<WeightedItem>(m_items);
            return Helpers.WeightedRandom<ItemID, WeightedItem>.GetWeightedItem(items);
        }

        public CharacterDescription GetSpecific()
        {
            return new CharacterDescription()
            {
                weapon = GetItemID(m_weapons),
                shield = GetItemID(m_shields),
                helmet = GetItemID(m_helmets),
                health = UnityEngine.Random.Range(m_healthRange.x, m_healthRange.y),
                scale = UnityEngine.Random.Range(m_scaleRange.x, m_scaleRange.y),
            };
        }
    }
}
