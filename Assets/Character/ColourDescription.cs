using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class ColourDescription
    {
        public int bodyColour;
        public int hairColour;
        public int weaponColour;
        public int shieldColour;
        public int helmetColour;
    }
}
