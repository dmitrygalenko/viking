using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "TeamSettingsList", menuName = "Settings/TeamSettingsList")]
    public class TeamSettingsList : ScriptableObject
    {
        [System.Serializable]
        public class TeamSettings
        {
            public GroupID groupID;
            public Color mainColour;
        }

        [SerializeField] private List<TeamSettings> m_settingsList = new List<TeamSettings>();

        public TeamSettings GetSettings(GroupID groupID)
        {
            return m_settingsList.Find(item => item.groupID == groupID);
        }
    }
}
