using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class AnimationCallbacks : MonoBehaviour
    {
        [SerializeField] private Character m_character;

        public void OnAttackActiveFramesStart()
        {
            m_character.OnAttackActiveFramesStart();
        }

        public void OnAttackActiveFramesEnd()
        {
            m_character.OnAttackActiveFramesEnd();
        }

        public void OnShoot()
        {
            m_character.OnShoot();
        }

        public void OnFootstep()
        {
            m_character.OnFootstep();
        }
    }
}
