using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainMenu
{
    public class MainMenuLogic : MonoBehaviour
    {
        [SerializeField] Gameplay.StartingCharacterStats m_charStats;
        [SerializeField] Gameplay.Character m_characterVisualImg;
        [SerializeField] Gameplay.GameplayParams m_gameParams;
        [SerializeField] SettingsScreen m_settingsScreen;
        [SerializeField] Gameplay.InputManager m_inputManager;
        [SerializeField] HintsDisplay m_hintText;

        private void Start()
        {
            Generate();
            m_inputManager.InitMenu(this);
            m_hintText.Init();
        }

        public void OnPlayPressed()
        {
            SceneManager.LoadScene("GameplayScene");
        }

        public void Generate()
        {
            var stats = m_charStats.GetDescription();
            m_gameParams.SetCharacterDescription(stats);
            m_characterVisualImg.Init(0, Gameplay.GroupID.Player, stats, colourDescription: null, services: null);
            m_gameParams.SetColourDescription(m_characterVisualImg.GetColours());
            m_characterVisualImg.SetActive(true);
        }

        public void OnClosePressed()
        {
            Application.Quit();
        }

        public void ShowSettingsScreen(bool show)
        {
            m_settingsScreen.Show(withInfo: true);
        }

        public void OnCancelPressed()
        {
            if (m_settingsScreen.IsShown())
            {
                m_settingsScreen.Hide();
            }
        }
    }
}
