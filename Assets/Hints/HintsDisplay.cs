using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainMenu
{
    public class HintsDisplay : MonoBehaviour
    {
        [SerializeField] private TMPro.TextMeshProUGUI m_messageText;
        [SerializeField] private Hints m_info;
        private Stack<int> m_shownHints;
        private int m_currentIdx;

        private void Awake()
        {
            m_shownHints = new Stack<int>();
        }

        public void Init()
        {
            UpdateHint();
        }

        public void OnNextHintPressed()
        {
            if (m_currentIdx >= 0)
                m_shownHints.Push(m_currentIdx);
            UpdateHint();
        }

        public void OnPreviousHintPressed()
        {
            if (m_shownHints.Count > 0)
            {
                m_currentIdx = m_shownHints.Pop();
                UpdateHint(m_currentIdx);
            }
            else
            {
                UpdateHint();
            }
        }

        private void UpdateHint(int forceIdx = -1)
        {
            if (forceIdx < 0)
                m_currentIdx = m_info.GetNextHintIndex(m_currentIdx);

            m_messageText.text = m_info.GetHint(m_currentIdx);
        }
    }
}
