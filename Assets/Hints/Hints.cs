using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainMenu
{
    [CreateAssetMenu(fileName = "Hints", menuName = "Settings/Hints")]
    public class Hints : ScriptableObject
    {
        [SerializeField] private string[] m_lines;

        public int GetNextHintIndex(int currentInd)
        {
            var result = -1;
            while (result < 0 || result == currentInd)
            {
                result = UnityEngine.Random.Range(0, m_lines.Length);
            }
            return result;
        }

        public string GetHint(int index)
        {
            return m_lines[index];
        }
    }
}
