using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Decor : MonoBehaviour, IVisuallySortable
    {
        [SerializeField] private ItemCustomisation m_customisation;
        [SerializeField] private ItemSettingsList m_settings;
        [SerializeField] private Animator m_animator;
        [SerializeField] private SortingLayerUpdater m_sortingUpdater;

        private class AnimatorParams
        {
            public int typeAnimParam = Animator.StringToHash("anim_type");
            public int isWalkRightTriggerParam = Animator.StringToHash("walkRight");
            public int isWalkLeftLTriggerParam = Animator.StringToHash("walkLeft");
        }

        private AnimatorParams m_animatorParameters = new AnimatorParams();

        public void Init(Services services)
        {
            if (m_customisation != null)
            {
                var colourInd = m_settings.GetRandomId();
                var settings = m_settings.GetSettings(colourInd);
                m_customisation.SetColour(settings.mainColour);
            }

            if (m_animator != null)
            {
                var type = UnityEngine.Random.Range(0, 3);
                m_animator.SetInteger(m_animatorParameters.typeAnimParam, type);
            }

            if (m_sortingUpdater != null)
            {
                m_sortingUpdater.SetSortableObject(this, services.settings);
            }
        }

        public void OnWalk(bool right)
        {
            if (m_animator != null)
            {
                var param = right ? m_animatorParameters.isWalkRightTriggerParam : m_animatorParameters.isWalkLeftLTriggerParam;
                m_animator.SetTrigger(param);
            }
        }

        public float GetBaseY()
        {
            return transform.position.y;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other == null)
                return;

            if (other.GetComponent<PositionTrigger>() == null)
                return;

            int triggerId = 0;
            if (other.transform.position.x < transform.position.x)
                triggerId = m_animatorParameters.isWalkRightTriggerParam;
            else
                triggerId = m_animatorParameters.isWalkLeftLTriggerParam;

            m_animator.SetTrigger(triggerId);
        }
    }
}
