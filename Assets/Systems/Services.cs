using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Services
    {
        public GameplayLogic gameplayLogic;
        public GameplaySettings settings;
        public ProjectileController projectilesController;
        public CharactersController charactersController;
        public InputManager inputManager;
        public CameraController cameraController;
        public LevelController levelController;
        public GroundFXController groundFXController;
        public AirVFXController airFXController;
    }
}
