using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Gameplay
{
    public class InputManager : MonoBehaviour
    {
        private Character m_controlledCharacter;
        private GameplayLogic m_gameplayLogic;
        private MainMenu.MainMenuLogic m_mainMenuLogic;
        private bool m_active;
        private bool m_isMenu;

        public void InitMenu(MainMenu.MainMenuLogic mainMenuLogic)
        {
            m_mainMenuLogic = mainMenuLogic;
            m_isMenu = true;
            m_active = true;
        }

        public void SetControlledCharacter(GameplayLogic gameplayLogic, Character character)
        {
            m_isMenu = false;
            m_gameplayLogic = gameplayLogic;
            m_controlledCharacter = character;
            m_active = true;
        }

        private bool IsMouseOverUI()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public void SetActive(bool value)
        {
            m_active = value;
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                if (m_isMenu)
                    m_mainMenuLogic.OnCancelPressed();
                else
                    m_gameplayLogic.OnCancelPressed();
            }

            if (!m_active || m_isMenu)
                return;

            if (m_controlledCharacter == null)
                return;

            if (Input.GetButtonDown("AttackL") || Input.GetButtonDown("AttackLMouse") && !IsMouseOverUI())
            {
                m_controlledCharacter.AttackL();
            }

            if (Input.GetButtonDown("AttackH") || Input.GetButtonDown("AttackOMouse") && !IsMouseOverUI())
            {
                m_controlledCharacter.AttackH();
            }

            if (Input.GetButtonDown("Cry"))
            {
                m_controlledCharacter.Cry();
            }

            m_controlledCharacter.Defend(Input.GetButton("Guard"));

            if (Input.GetButtonDown("Roll"))
            {
                // m_controlledCharacter.Roll();
            }

            if (Input.GetButtonDown("Action"))
            {
                m_controlledCharacter.Action();
            }

            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");
            m_controlledCharacter.Move(horizontalInput, verticalInput);
        }
    }
}
