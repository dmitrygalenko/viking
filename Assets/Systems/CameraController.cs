using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Camera m_camera;
        [SerializeField] private float m_transitionSpeed;
        [SerializeField] private float m_shakeFalloff;
        private float m_baseY;
        private Transform m_objectToFollow;
        private bool m_inTransition;
        private float m_currentAmplitude;
        [SerializeField] private float m_timeScale;
        private float m_phase;

        public enum ShakeIntensity
        {
            Small,
            Medium,
            Large,
        }

        [System.Serializable]
        private class CameraShakeIntensty
        {
            public ShakeIntensity intensity;
            public float amplitude;
        }

        [SerializeField] private List<CameraShakeIntensty> m_intensities;

        public void Init()
        {
            m_phase = 0f;
            m_objectToFollow = null;
            m_baseY = m_camera.transform.position.y;
        }

        public void SetObject(Transform transform, bool instant = false)
        {
            m_objectToFollow = transform;
            m_inTransition = !instant;
        }

        public void UpdateController(float timeDelta)
        {
            UpdateShake(timeDelta);

            if (m_objectToFollow != null)
            {
                if (m_inTransition)
                {
                    MoveToTarget(m_objectToFollow.position.x, timeDelta);
                }
                else
                {
                    SetCameraPosition(m_objectToFollow.position.x);
                }
            }
        }

        private void MoveToTarget(float x, float dTime)
        {
            var currentPosition = m_camera.transform.position.x;
            var direction = x - currentPosition;
            var sign = Mathf.Sign(direction);
            var distance = m_transitionSpeed * sign;
            if (Mathf.Abs(distance) > Mathf.Abs(direction))
                distance = direction;

            SetCameraPosition(currentPosition + distance);

            if (Mathf.Abs(m_camera.transform.position.x - x) < 0.01f)
            {
                m_inTransition = false;
            }
        }

        private void SetCameraPosition(float x)
        {
            var cameraPosition = m_camera.transform.position;
            cameraPosition.x = x;
            m_camera.transform.position = cameraPosition;
        }

        public void AddShake(ShakeIntensity intensity)
        {
            var param = m_intensities.Find(item => item.intensity == intensity);
            var shakeAmplitude = param.amplitude;
            if (shakeAmplitude > m_currentAmplitude)
                m_currentAmplitude = shakeAmplitude;
        }

        public float GetCameraX()
        {
            return m_camera.transform.position.x;
        }

        private void UpdateShake(float timeDelta)
        {
            if (m_currentAmplitude > 0.01f)
            {
                var cameraPosition = m_camera.transform.position;
                cameraPosition.y = m_baseY + m_currentAmplitude * Mathf.Sin(m_phase);
                m_camera.transform.position = cameraPosition;
                m_phase = m_phase + timeDelta * m_timeScale;
                if (m_phase > Mathf.PI * 2f)
                    m_phase -= Mathf.PI * 2f;

                m_currentAmplitude -= m_shakeFalloff * timeDelta;
            }
        }
    }
}
