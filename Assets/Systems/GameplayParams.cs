using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "GameplayParams", menuName = "Settings/GameplayParams")]
    public class GameplayParams : ScriptableObject
    {
        private CharacterDescription m_mainCharacterDescription;
        private ColourDescription m_colourDescription;

        public CharacterDescription GetCharacterDescription()
        {
            return m_mainCharacterDescription;
        }

        public ColourDescription GetColourDescription()
        {
            return m_colourDescription;
        }

        public void SetCharacterDescription(CharacterDescription characterDescription)
        {
            m_mainCharacterDescription = characterDescription;
        }

        public void SetColourDescription(ColourDescription colourDescription)
        {
            m_colourDescription = colourDescription;
        }
    }
}
