using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Item : MonoBehaviour
    {
        [SerializeField] protected ItemType m_itemType;
        [SerializeField] protected ItemID m_itemID;
        [SerializeField] private SpriteRenderer m_sprite = default;
        [SerializeField] private ItemCustomisation m_itemCustomisation = default;
        [SerializeField] private ItemSettingsList m_itemSettings = default;
        [SerializeField] private CollidingItem[] m_collidingParts = default;
        protected float m_maxHealth;
        protected float m_health;
        protected Character m_character;
        protected int m_playerId;
        private int m_colourInd;

        private void OnValidate()
        {
            m_collidingParts = GetComponentsInChildren<CollidingItem>();
            m_sprite = GetComponent<SpriteRenderer>();
        }

        public virtual void Init(int playerId, Character character, Services services, int colourInd)
        {
            m_playerId = playerId;
            m_character = character;
            for (int i = 0; i < m_collidingParts.Length; ++i)
            {
                m_collidingParts[i].Init(this, playerId, m_character, services);
            }

            if (m_itemCustomisation != null)
            {
                if (colourInd >= 0)
                    m_colourInd = colourInd;
                else
                    m_colourInd = m_itemSettings.GetRandomId();
                var itemSettings = m_itemSettings.GetSettings(m_colourInd);
                m_itemCustomisation.SetColour(itemSettings.mainColour);
            }
        }

        public int GetColourInd()
        {
            return m_colourInd;
        }

        public ItemType GetItemType()
        {
            return m_itemType;
        }

        public int GetPlayerID()
        {
            return m_playerId;
        }

        public Character GetCharacter()
        {
            return m_character;
        }

        public ItemID GetItemID()
        {
            return m_itemID;
        }

        public virtual void OnGetHit(int otherPlayerId, Item hittingItem, float damage)
        {
            m_character.OnGetHit(otherPlayerId, hittingItem, this, damage);
        }

        public void EnableColliders(bool active)
        {
            for (int i = 0; i < m_collidingParts.Length; ++i)
            {
                m_collidingParts[i].EnableColliders(active);
            }
        }

        public virtual void OnCollideWith(Item another)
        {

        }

        protected void TakeDamage(float damage)
        {
            m_health = Mathf.Max(0f, m_health - damage);
            m_character.UpdateItemHealth(this, damage);
        }

        public bool IsWorking()
        {
            return m_health > 0f;
        }

        public float GetHealthRatio()
        {
            return m_health / m_maxHealth;
        }

        public virtual bool IsEmpty()
        {
            return m_itemID == ItemID.None;
        }

        public virtual float GetBaseY()
        {
            return m_character.GetBaseY();
        }

        public void Tint(float red)
        {
            // if (m_sprite != null)
            // {
            //     var itemColour = m_sprite.color;
            //     m_sprite.color = new Color(Mathf.Clamp(itemColour.r + red, 0f, 0.6f),
            //     Mathf.Clamp(itemColour.g - red, 0.1f, 1f),
            //     Mathf.Clamp(itemColour.b - red, 0.1f, 1f), 1f);
            // }
        }
    }
}
