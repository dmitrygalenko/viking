using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public enum ItemID
    {
        None,
        WeaponSword1,
        WeaponSword2,
        WeaponAxe,
        WeaponKnife,
        WeaponCrossbow,
        WeaponPike,
        ShieldWooden,
        ShieldRoundWooden,
        ShieldMetal,
        HelmetSimple,
        HelmetCone,
        HelmetFancy,
    }
}
