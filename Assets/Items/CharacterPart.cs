using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CharacterPart : MonoBehaviour
    {
        public enum ItemType
        {
            Body,
            Weapon,
            Shield,
            Ammo,
        }

        [SerializeField] private List<Item> m_items = default;
        protected Item m_currentItem;
        private int m_playerId;
        Character m_character;
        Services m_services;

        public void Init(int playerId, Character character, Services services)
        {
            m_playerId = playerId;
            m_character = character;
            m_services = services;
        }

        public void SetRandomItem()
        {
            var itemIdx = UnityEngine.Random.Range(0, m_items.Count);
            SetItem(m_items[itemIdx].GetItemID(), -1);
        }

        public virtual void SetItem(ItemID itemID, int colourInd)
        {
            for (int i = 0; i < m_items.Count; ++i)
            {
                var isSelected = m_items[i].GetItemID() == itemID;
                m_items[i].gameObject.SetActive(isSelected);
                if (isSelected)
                {
                    m_currentItem = m_items[i];
                }
            }

            if (m_currentItem == null)
            {
                Debug.LogError($"no item matches ID {itemID}");
                return;
            }

            m_currentItem.Init(m_playerId, m_character, m_services, colourInd);
        }

        public void EnableColliders(bool active)
        {
            m_currentItem.EnableColliders(active);
        }

        public bool IsWorking()
        {
            return m_currentItem.IsWorking();
        }

        public float GetHealthRatio()
        {
            return m_currentItem.GetHealthRatio();
        }

        public bool IsEmpty()
        {
            return m_currentItem.IsEmpty();
        }

        public Item GetCurrentItem()
        {
            return m_currentItem;
        }
    }
}
