using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class ShieldItem : Item
    {
        [SerializeField] private float m_maxShieldHealth;

        override public void Init(int playerId, Character character, Services services, int colourInd)
        {
            m_maxHealth = m_maxShieldHealth;
            m_health = m_maxHealth;
            base.Init(playerId, character, services, colourInd);
        }

        override public void OnGetHit(int otherPlayerId, Item hittingItem, float damage)
        {
            if (IsEmpty())
            {
                base.OnGetHit(otherPlayerId, hittingItem, damage);
                return;
            }

            TakeDamage(damage);
            m_character.OnGetHit(otherPlayerId, hittingItem, this, 0f);
        }
    }
}
