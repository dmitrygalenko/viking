using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Arrow : Item, IVisuallySortable, IPoolableItem
    {
        private float m_speed;
        private bool m_isAttacking;
        [SerializeField] private float m_maxTime = 3f;
        [SerializeField] private float m_stuckTime = 3f;
        private Vector3 m_direction;
        private float m_time;
        private float m_damage;
        [SerializeField] private Transform[] m_visuals;
        [SerializeField] private SortingLayerUpdater m_layerUpdater;
        private bool m_canDestroy;

        override public void Init(int playerId, Character character, Services services, int colourInd)
        {
            base.Init(playerId, character, services, colourInd);
            m_canDestroy = false;
            StartAttack();
            m_time = 0f;
            m_layerUpdater.SetSortableObject(this, services.settings);
        }

        public void InitArrow(Vector3 direction, float damage, float speed)
        {
            m_direction = direction;
            m_speed = speed;
            m_damage = damage;
            Direct();
        }

        private void Direct()
        {
            var angle = Mathf.Atan2(m_direction.y, m_direction.x) * 180f / Mathf.PI;

            for (int i = 0; i < m_visuals.Length; ++i)
            {
                m_visuals[i].transform.localRotation = Quaternion.Euler(0f, 0f, angle);
            }
        }

        override public void OnCollideWith(Item anotherItem)
        {
            if (m_isAttacking)
            {
                anotherItem.OnGetHit(m_playerId, this, m_damage);
                EndAttack();
                Stick(anotherItem);
            }
        }

        override public float GetBaseY()
        {
            return transform.position.y;
        }

        public void StartAttack()
        {
            m_isAttacking = true;
            EnableColliders(true);
        }

        private void EndAttack()
        {
            m_isAttacking = false;
            EnableColliders(false);
        }

        public void UpdateArrow(float timeDelta)
        {
            m_time += timeDelta;
            if (m_isAttacking)
            {
                if (m_time >= m_maxTime)
                {
                    m_isAttacking = false;
                    Stop();
                    return;
                }

                transform.Translate(m_direction * m_speed);
            }
            else
            {
                if (m_time >= m_stuckTime)
                {
                    Stop();
                }
            }
        }

        private void Stick(Item anotherItem)
        {
            m_time = 0f;
            gameObject.transform.parent = anotherItem.transform;
        }

        private void Stop()
        {
            m_canDestroy = true;
        }

        public void Reset()
        {
            gameObject.SetActive(false);
        }

        public void InitFromPool()
        {
            gameObject.SetActive(true);
        }

        public bool ShouldRemoveToPool()
        {
            return m_canDestroy;
        }
    }
}
