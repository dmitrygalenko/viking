using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class ItemCustomisation : MonoBehaviour
    {
        [SerializeField] List<SpriteRenderer> m_spriteRenderers = new List<SpriteRenderer>();

        public void SetColour(Color colour)
        {
            for (int i = 0; i < m_spriteRenderers.Count; ++i)
            {
                m_spriteRenderers[i].color = colour;
            }
        }
    }
}
