using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public enum ItemType
    {
        Body,
        Weapon,
        Shield,
        Ammo,
        Helmet,
    }
}
