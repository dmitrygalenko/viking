using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CharacterPartWeapon : CharacterPart
    {
        private WeaponItem m_currentWeapon;

        override public void SetItem(ItemID itemID, int colourInd)
        {
            base.SetItem(itemID, colourInd);
            m_currentWeapon = m_currentItem.GetComponent<WeaponItem>();
        }

        public WeaponItem GetCurrentWeapon()
        {
            return m_currentWeapon;
        }
    }
}
