using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class WeaponItem : Item
    {
        [SerializeField] private float m_damage;
        [SerializeField] private float m_distance;
        [SerializeField] private float m_attackSpeed = 1f;
        [SerializeField] private bool m_isRanged;
        [SerializeField] private Transform m_shootingPoint;
        [SerializeField] private Arrow m_projectile;
        [SerializeField] private float m_arrowDamage;
        [SerializeField] private float m_speed;
        [SerializeField] private int m_maxAmmo;
        private bool m_isAttacking;
        private int m_ammoLeft;

        override public void Init(int playerId, Character character, Services services, int colourInd)
        {
            base.Init(playerId, character, services, colourInd);
            m_ammoLeft = m_maxAmmo;
        }

        override public void OnCollideWith(Item anotherItem)
        {
            if (m_isAttacking)
            {
                m_character.OnHitAnother(anotherItem.GetCharacter(), anotherItem.GetItemType(), m_damage);
                anotherItem.OnGetHit(m_playerId, this, m_damage);
            }
        }

        public bool IsRanged()
        {
            return m_isRanged;
        }

        public int GetAmmoLeft()
        {
            return m_ammoLeft;
        }

        public void StartAttack()
        {
            m_isAttacking = true;
            EnableColliders(true);
        }

        public void EndAttack()
        {
            m_isAttacking = false;
            EnableColliders(false);
        }

        public void Shoot(Vector3 direction, Services services)
        {
            if (m_ammoLeft <= 0)
                return;

            var position = m_character.transform.position;
            position.x = m_shootingPoint.transform.position.x;
            services.projectilesController.Shoot(m_projectile, position, direction, speed: m_speed, m_arrowDamage, m_character);
            --m_ammoLeft;
        }

        public float GetRecommendedDistance()
        {
            return m_distance;
        }

        public float GetAttackSpeed()
        {
            return m_attackSpeed;
        }
    }
}
