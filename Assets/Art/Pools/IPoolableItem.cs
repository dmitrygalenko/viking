using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public interface IPoolableItem
    {
        void Reset();
        void InitFromPool();
        bool ShouldRemoveToPool();
    }
}
