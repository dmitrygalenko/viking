using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class PoolController<T> : MonoBehaviour where T : Object, IPoolableItem
    {
        [SerializeField] protected Transform m_container;
        [SerializeField] private T m_prefab;
        [SerializeField] private int m_prespawnAmount = 5;
        protected List<T> m_activeItems;
        private Stack<T> m_itemsPool;

        public void Init()
        {
            m_activeItems = new List<T>();
            m_itemsPool = new Stack<T>();
            Clear();
            for (int i = 0; i < m_prespawnAmount; ++i)
            {
                var item = NewItem();
                AddToPool(item);
            }
        }

        private void Clear()
        {
            foreach (Transform child in m_container)
            {
                GameObject.Destroy(child.gameObject);
            }

            m_activeItems.Clear();
            m_itemsPool.Clear();
        }

        public T GetItemFromPool()
        {
            T item = default(T);
            if (m_itemsPool.Count > 0)
            {
                item = m_itemsPool.Pop();
            }
            else
            {
                item = NewItem();
            }
            m_activeItems.Add(item);
            item.InitFromPool();
            return item;
        }

        private T NewItem()
        {
            var item = GameObject.Instantiate(m_prefab, m_container);
            return item;
        }

        void AddToPool(T item)
        {
            m_itemsPool.Push(item);
            item.Reset();
        }

        public void ReturnToPool(T item)
        {
            AddToPool(item);
            m_activeItems.Remove(item);
        }
    }
}
