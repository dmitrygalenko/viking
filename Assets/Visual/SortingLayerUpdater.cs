using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class SortingLayerUpdater : MonoBehaviour
    {
        [SerializeField] private List<SpriteRenderer> m_allRenderers;
        private List<int> m_allBaseSortingOrders;
        private IVisuallySortable m_sortableObject;
        private float m_step;
        [SerializeField] private bool m_once = false;
        private bool m_inited = false;

        private void OnValidate()
        {
            m_allRenderers = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>(includeInactive: true));
        }

        private void Awake()
        {
            InitSortingOrder();
        }

        private void InitSortingOrder()
        {
            m_allBaseSortingOrders = new List<int>(m_allRenderers.Count);
            for (int i = 0; i < m_allRenderers.Count; ++i)
            {
                m_allBaseSortingOrders.Add(m_allRenderers[i].sortingOrder);
            }
            m_inited = true;
        }

        public void SetSortableObject(IVisuallySortable sortable, GameplaySettings gameplaySettings)
        {
            m_step = gameplaySettings.GetOrderingStep();
            m_sortableObject = sortable;
            if (m_once)
            {
                Sort();
            }
        }

        private void LateUpdate()
        {
            if (!m_once)
            {
                Sort();
            }
        }

        private void Sort()
        {
            if (m_sortableObject == null)
                return;

            if (!m_inited)
            {
                InitSortingOrder();
            }

            var y = m_sortableObject.GetBaseY();
            for (int i = 0; i < m_allRenderers.Count; ++i)
            {
                m_allRenderers[i].sortingOrder = m_allBaseSortingOrders[i] - Mathf.RoundToInt(y * m_step);
            }
        }
    }
}
