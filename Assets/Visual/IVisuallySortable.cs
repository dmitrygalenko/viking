using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IVisuallySortable
{
    public float GetBaseY();
}
