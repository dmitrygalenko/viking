using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class MapObjectScaler : MonoBehaviour
    {
        [SerializeField] private Transform m_targetTransform;
        private float m_originalScale = 1f;
        private Vector2 m_scaleCoefficient;
        private Services m_services;

        public void Init(Services services)
        {
            m_services = services;
            if (m_services != null)
                m_scaleCoefficient = m_services.settings.GetBaseMapScale();
        }

        public void SetBaseScale(float scale)
        {
            m_originalScale = scale;
        }

        private void LateUpdate()
        {
            var y = m_targetTransform.position.y;
            var scale = m_originalScale * (m_scaleCoefficient.x * y + m_scaleCoefficient.y);
            m_targetTransform.localScale = new Vector3(scale, scale, 0f);
        }
    }
}
