using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class AirVFXController : MonoBehaviour
    {
        [SerializeField] private GameObject[] m_rainObjects;
        [SerializeField] private float m_rainStep;
        [SerializeField] private float m_rainThreshold;
        [SerializeField] private GameObject m_windObject;
        private Camera m_camera;
        private int m_currentRainIndex;
        private float m_coveredDirection;
        public void Init(Camera camera)
        {
            m_camera = camera;
        }

        public void UpdateController(float timeDelta)
        {
            var otherRainIndex = (m_currentRainIndex + 1) % m_rainObjects.Length;
            if (m_camera.transform.position.x < m_rainObjects[m_currentRainIndex].transform.position.x - m_rainThreshold)
            {
                if (m_coveredDirection >= 0f)
                {
                    var rainPos = m_rainObjects[otherRainIndex].transform.position;
                    rainPos.x = m_rainObjects[m_currentRainIndex].transform.position.x - m_rainStep;
                    m_rainObjects[otherRainIndex].transform.position = rainPos;
                    m_coveredDirection = -1f;
                }
            }
            else if (m_camera.transform.position.x > m_rainObjects[m_currentRainIndex].transform.position.x + m_rainThreshold)
            {
                if (m_coveredDirection <= 0f)
                {
                    var rainPos = m_rainObjects[otherRainIndex].transform.position;
                    rainPos.x = m_rainObjects[m_currentRainIndex].transform.position.x + m_rainStep;
                    m_rainObjects[otherRainIndex].transform.position = rainPos;
                    m_coveredDirection = 1f;
                }
            }

            if ((m_camera.transform.position.x < m_rainObjects[m_currentRainIndex].transform.position.x - m_rainStep * 0.5f)
            || (m_camera.transform.position.x > m_rainObjects[m_currentRainIndex].transform.position.x + m_rainStep * 0.5f))
            {
                m_currentRainIndex = otherRainIndex;
            }

            var windPos = m_windObject.transform.position;
            windPos.x = m_camera.transform.position.x;
            m_windObject.transform.position = windPos;
        }
    }
}
