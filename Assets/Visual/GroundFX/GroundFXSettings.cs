using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "GroundFXSettings", menuName = "Settings/GroundFXSettings")]
    public class GroundFXSettings : ScriptableObject
    {
        [SerializeField] private float m_visualTime;
        [SerializeField] private float m_fadeStep;

        public float GetVisualTime()
        {
            return m_visualTime;
        }

        public float GetFadeStep()
        {
            return m_fadeStep;
        }
    }
}
