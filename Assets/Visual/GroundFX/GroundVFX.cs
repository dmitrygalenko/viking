using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class GroundVFX : MonoBehaviour, IPoolableItem
    {
        private SpriteRenderer m_sprite;
        [SerializeField] private float m_initAlpha;
        [SerializeField] private SpriteRenderer[] m_visuals;

        public void Init()
        {
            var chosenVisualId = UnityEngine.Random.Range(0, m_visuals.Length);
            for (int i = 0; i < m_visuals.Length; ++i)
                m_visuals[i].gameObject.SetActive(chosenVisualId == i);
            m_sprite = m_visuals[chosenVisualId];

            SetAlpha(m_initAlpha);
        }

        private void SetAlpha(float value)
        {
            var colour = m_sprite.color;
            colour.a = value;
            m_sprite.color = colour;
        }

        public void StepAlpha(float value)
        {
            SetAlpha(Mathf.Max(0f, m_sprite.color.a - value));
        }

        public void Reset()
        {
            gameObject.SetActive(false);
        }

        public void InitFromPool()
        {
            gameObject.SetActive(true);
        }

        public bool ShouldRemoveToPool()
        {
            return false;
        }
    }
}
