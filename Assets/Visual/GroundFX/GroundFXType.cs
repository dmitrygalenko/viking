using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public enum GroundFXType
    {
        Blood,
        Footstep,
        BattleMark,
    }
}
