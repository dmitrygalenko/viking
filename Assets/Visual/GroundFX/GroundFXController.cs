using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class GroundFXController : MonoBehaviour
    {
        [SerializeField] private GroundFXSettings m_settings;
        [System.Serializable]
        public class PoolVFXSettings
        {
            public GroundFXType type;
            public GroundFXPoolController pool;
        }
        [SerializeField] private List<PoolVFXSettings> m_pools;
        private float m_time;

        private class GroundFXItem
        {
            public GroundFXType type;
            public GroundVFX groundFX;
            public float time;
        }
        private List<GroundFXItem> m_vfxItems;
        private float m_alphaStep;

        public void Init()
        {
            m_vfxItems = new List<GroundFXItem>(100);
            m_alphaStep = m_settings.GetFadeStep() / m_settings.GetVisualTime();

            for (int i = 0; i < m_pools.Count; ++i)
            {
                m_pools[i].pool.Init();
            }
        }

        public void CreateVFX(GroundFXType Type, Vector3 position, float xOrientation)
        {
            var poolData = m_pools.Find(item => item.type == Type);
            var vfxItem = poolData.pool.GetItemFromPool();
            vfxItem.transform.position = position;
            vfxItem.transform.localScale = new Vector3(Mathf.Sign(xOrientation), 1f, 1f);
            vfxItem.Init();
            m_vfxItems.Add(new GroundFXItem()
            {
                type = Type,
                groundFX = vfxItem,
                time = m_settings.GetVisualTime(),
            });
        }

        public void UpdateVFX(float timeDelta)
        {
            for (int i = 0; i < m_vfxItems.Count; ++i)
            {
                m_vfxItems[i].time -= timeDelta;
                if (m_vfxItems[i].time <= 0f)
                {
                    var poolData = m_pools.Find(item => item.type == m_vfxItems[i].type);
                    poolData.pool.ReturnToPool(m_vfxItems[i].groundFX);
                }
            }

            m_vfxItems.RemoveAll(item => item.time <= 0f);

            m_time += timeDelta;
            if (m_time >= m_settings.GetFadeStep())
            {
                m_time = 0f;

                for (int i = 0; i < m_vfxItems.Count; ++i)
                {
                    m_vfxItems[i].groundFX.StepAlpha(m_alphaStep);
                }
            }
        }
    }
}
