using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [System.Serializable]
    public class Reward
    {
        public ItemType itemType;
        public ItemID itemId;
        public float health;
        public int XP;
        public int colourInd;
    }
}
