using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Pickup : MonoBehaviour, IVisuallySortable
    {
        [SerializeField] private Loot[] m_loot;
        [SerializeField] private Decor[] m_decor;
        private Loot m_chosenLoot;
        [SerializeField] private GameObject m_message;
        [SerializeField] private Collider2D m_collider;
        [SerializeField] private SortingLayerUpdater m_sortingUpdater;

        public void Init(Services services)
        {
            m_message.gameObject.SetActive(false);

            for (int i = 0; i < m_decor.Length; ++i)
            {
                var isChosen = m_decor[i].gameObject.activeSelf;
                if (isChosen)
                    m_decor[i].Init(services);
            }

            for (int i = 0; i < m_loot.Length; ++i)
            {
                var isChosen = m_loot[i].gameObject.activeSelf;
                if (isChosen)
                {
                    m_chosenLoot = m_loot[i];
                    m_chosenLoot.Init();
                }
            }

            m_collider.enabled = true;

            m_sortingUpdater.SetSortableObject(this, services.settings);
        }

        public void OnEnteredAction()
        {
            ShowMessage(true);
        }

        public void OnLeftAction()
        {
            ShowMessage(false);
        }

        private void ShowMessage(bool active)
        {
            m_message.gameObject.SetActive(active);
        }

        public Reward GetReward()
        {
            var reward = m_chosenLoot.GetReward();
            m_chosenLoot.gameObject.SetActive(false);
            m_chosenLoot = null;
            m_collider.enabled = false;

            return reward;
        }

        public float GetBaseY()
        {
            return transform.position.y;
        }
    }
}
