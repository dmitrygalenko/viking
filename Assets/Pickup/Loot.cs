using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Loot : MonoBehaviour
    {
        [SerializeField] private Reward m_reward;
        [SerializeField] private ItemCustomisation m_itemCustomisation;
        [SerializeField] private ItemSettingsList m_itemSettings;

        public void Init()
        {
            var colourInd = m_itemSettings.GetRandomId();
            m_reward.colourInd = colourInd;
            var settings = m_itemSettings.GetSettings(colourInd);
            m_itemCustomisation.SetColour(settings.mainColour);
        }

        public Reward GetReward()
        {
            return m_reward;
        }
    }
}
