using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(fileName = "GameplaySettings", menuName = "Settings/GameplaySettings")]
    public class GameplaySettings : ScriptableObject
    {
        [SerializeField] private float m_verticalHitDistance = 1f;
        [SerializeField] private float m_YToXRatio;
        [SerializeField] private float m_orderingStep = 20f;
        [SerializeField] private Vector2 m_mapScaleBase;
        [SerializeField] private float m_minVerticalTargeting;
        [SerializeField] private float m_characterSpawnDistance;
        [SerializeField] private float m_stageLimitDistance;
        [SerializeField] private float m_despawnTime = 15f;
        [SerializeField] private float m_xBlockPath = 1.5f;
        [SerializeField] private float m_yBlockPath = 0.4f;
        [SerializeField] private float m_pushbackAttacker = 0.04f;
        [SerializeField] private float m_pushbackAttacked = 0.1f;
        public enum CooldownID
        {
            LightAttack,
            HeavyAttack,
            EvasiveRoll,
            GetHit,
            BlockHit,
            Defend,
            Cry,
            OnAttackBlocked,
        }

        [System.Serializable]
        public class CooldownInfo
        {
            public CooldownID id;
            public float time;
        }

        [SerializeField] private List<CooldownInfo> m_cooldowns;

        public float GetVerticalHitDistance()
        {
            return m_verticalHitDistance;
        }

        public Vector2 GetBaseMapScale()
        {
            return m_mapScaleBase;
        }

        public float GetOrderingStep()
        {
            return m_orderingStep;
        }

        public float GetMinVerticalTargeting()
        {
            return m_minVerticalTargeting;
        }

        public float GetCharacterSpawnDistance()
        {
            return m_characterSpawnDistance;
        }

        public float GetStageLimitDistance()
        {
            return m_stageLimitDistance;
        }

        public float GetDespawnTime()
        {
            return m_despawnTime;
        }

        public float GetYToXRatio()
        {
            return m_YToXRatio;
        }

        public float GetCooldownTimer(CooldownID ID)
        {
            var info = m_cooldowns.Find(item => item.id == ID);
            if (info == null)
                return 0f;

            return info.time;
        }

        public float GetXPathBlock()
        {
            return m_xBlockPath;
        }

        public float GetYPathBlock()
        {
            return m_yBlockPath;
        }

        public float GetPushbackDistance(bool isAttacker)
        {
            return isAttacker ? m_pushbackAttacker : m_pushbackAttacked;
        }
    }
}
