using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CollidingItem : MonoBehaviour
    {
        private Item m_item;
        private int m_playerId;

        private Character m_character;
        [SerializeField] private Collider2D m_collider;
        private Services m_services;

        private void OnValidate()
        {
            m_collider = GetComponent<Collider2D>();
        }

        public void Init(Item item, int playerId, Character character, Services services)
        {
            m_services = services;
            m_item = item;
            m_playerId = playerId;
            m_character = character;
        }

        public Item GetItem()
        {
            return m_item;
        }

        public int GetPlayerId()
        {
            return m_playerId;
        }

        public float GetBaseY()
        {
            return m_item.GetBaseY();
        }

        public void EnableColliders(bool enable)
        {
            m_collider.enabled = enable;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var otherCollidingItem = other.GetComponent<CollidingItem>();
            if (otherCollidingItem != null)
            {
                if (otherCollidingItem.GetPlayerId() == GetPlayerId())
                    return;

                var otherY = otherCollidingItem.GetBaseY();
                var thisY = GetBaseY();
                if (Mathf.Abs(otherY - thisY) > m_services.settings.GetVerticalHitDistance())
                    return;

                m_item.OnCollideWith(otherCollidingItem.GetItem());
                return;
            }
        }

        public Character GetCharacter()
        {
            return m_character;
        }
    }
}
