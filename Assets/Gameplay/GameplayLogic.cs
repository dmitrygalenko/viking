using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay
{
    public class GameplayLogic : MonoBehaviour
    {
        private Character m_mainCharacter;
        [SerializeField] private List<Character> m_enemies;
        [SerializeField] private List<Character> m_allies;
        [SerializeField] private Camera m_camera;
        [SerializeField] private GameplaySettings m_settings;
        [Header("Controllers")]
        [SerializeField] private ProjectileController m_projectilesController;
        [SerializeField] private CharactersController m_charactersController;
        [SerializeField] private CameraController m_cameraController;
        [SerializeField] private LevelController m_level;
        [SerializeField] private GroundFXController m_groundFXController;
        [SerializeField] private AirVFXController m_airFXController;
        [SerializeField] private InputManager m_inputManager;
        [Header("Starting Parameters")]
        [SerializeField] private GameplayParams m_gameParams;
        [SerializeField] private StartingCharacterStats m_debugStartingCharacterStarts;
        [Header("Menus")]
        [SerializeField] private GameplayMenu.GameplayMenu m_menu;
        [SerializeField] private MainMenu.SettingsScreen m_settingsScreen;
        private bool m_active;
        private Services m_services;
        [SerializeField] private float m_resultTimer = 3f;
        private float m_menuTimer;
        private bool m_won;

        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            m_services = new Services()
            {
                gameplayLogic = this,
                airFXController = m_airFXController,
                cameraController = m_cameraController,
                charactersController = m_charactersController,
                groundFXController = m_groundFXController,
                inputManager = m_inputManager,
                levelController = m_level,
                projectilesController = m_projectilesController,
                settings = m_settings,
            };

            m_services.groundFXController.Init();
            m_services.airFXController.Init(m_camera);
            m_services.projectilesController.Init(m_services);
            m_services.charactersController.Init(m_services);
            m_services.cameraController.Init();
            m_menuTimer = 0f;
            CharacterDescription startingCharacterDescription = m_gameParams.GetCharacterDescription();
            ColourDescription startingColourDescription = m_gameParams.GetColourDescription();
            // for running the scene separately
            if (startingCharacterDescription == null)
            {
                startingCharacterDescription = m_debugStartingCharacterStarts.GetDescription();
            }
            m_level.Init(startingCharacterDescription, startingColourDescription, m_services);
            m_mainCharacter = m_level.GetMainCharacter();
            m_services.inputManager.SetControlledCharacter(this, m_mainCharacter);
            m_services.cameraController.SetObject(m_mainCharacter.transform, instant: true);
            m_menu.gameObject.SetActive(true);
            m_menu.Init(gameplayLogic: this);
            m_settingsScreen.gameObject.SetActive(false);
            m_active = true;
        }

        public void Close()
        {
            SceneManager.LoadScene("MainMenu");
        }

        public void Restart()
        {
            SceneManager.LoadScene("GameplayScene");
        }

        public void OnCancelPressed()
        {
            if (m_menuTimer > 0f)
                return;

            if (m_settingsScreen.IsShown())
            {
                ShowSettings(false);
            }
            else if (!m_menu.IsShown())
            {
                SetActive(false);
                m_menu.Show(GameplayMenu.GameplayMenu.MenuType.Pause);
            }
            else if (m_menu.IsShown() && m_menu.CanHide())
            {
                SetActive(true);
                m_menu.RequestHide();
            }
        }

        public void Menu()
        {
            if (!m_menu.IsShown())
            {
                SetActive(false);
                m_menu.Show(GameplayMenu.GameplayMenu.MenuType.Pause);
                return;
            }
        }

        public void ShowSettings(bool show)
        {
            if (show)
            {
                m_settingsScreen.gameObject.SetActive(true);
                m_settingsScreen.Show(withInfo: false);
            }
            else
            {
                m_settingsScreen.Hide();
            }
        }

        public void SetActive(bool value, bool keepAnimations = false)
        {
            m_active = value;
            m_services.inputManager.SetActive(value);
            m_charactersController.SetActive(value, keepAnimations);
        }

        private void Update()
        {
            var timeDelta = Time.deltaTime;
            if (m_active)
            {
                m_services.groundFXController.UpdateVFX(timeDelta);
                m_services.charactersController.UpdateController(timeDelta);
                m_services.projectilesController.UpdateController(timeDelta);
                m_services.cameraController.UpdateController(timeDelta);
                m_services.levelController.UpdateController(timeDelta);
                m_services.airFXController.UpdateController(timeDelta);
            }

            UpdateMenu(timeDelta);
        }

        public void OnLost()
        {
            SetActive(false, keepAnimations: true);
            m_menuTimer = m_resultTimer;
            m_won = false;
        }

        public void OnLevelComplete()
        {
            SetActive(false, keepAnimations: true);
            m_menuTimer = m_resultTimer;
            m_won = true;
        }

        private void UpdateMenu(float deltaTime)
        {
            if (m_menuTimer > 0f)
            {
                m_menuTimer -= deltaTime;
                if (m_menuTimer <= 0f)
                {
                    var result = m_won ? GameplayMenu.GameplayMenu.MenuType.Won : GameplayMenu.GameplayMenu.MenuType.Lost;
                    m_menu.Show(result);
                }
            }
        }
    }
}
