using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class ProjectileController : PoolController<Arrow>
    {
        private Services m_services;

        public void Init(Services services)
        {
            base.Init();
            m_services = services;
        }

        public void Shoot(Arrow projectile, Vector3 position, Vector3 direction, float speed, float damage, Character character)
        {
            var arrow = GetItemFromPool();
            arrow.transform.position = position;
            arrow.Init(character.GetPlayerID(), character, m_services, -1);
            arrow.InitArrow(direction, damage, speed: speed);
        }

        public void UpdateController(float timeDelta)
        {
            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                m_activeItems[i].UpdateArrow(timeDelta);
            }

            int idx = 0;
            while (idx < m_activeItems.Count)
            {
                if (m_activeItems[idx].ShouldRemoveToPool())
                {
                    ReturnToPool(m_activeItems[idx]);
                }
                else
                {
                    ++idx;
                }
            }
        }
    }
}
