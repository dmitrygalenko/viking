using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CharactersController : PoolController<Character>
    {
        [SerializeField] private AI.AISettings m_AICharacterSettings;
        private Character m_mainCharacter;
        private Services m_services;
        private int m_nextPlayerID;
        private Vector2 m_xMovementLimit;
        private Vector2 m_yMovementLimit;

        public void Init(Services services)
        {
            base.Init();

            m_services = services;
            m_nextPlayerID = 0;
        }

        public Character AddCharacter(CharacterDescription info, ColourDescription colourInfo, GroupID groupID, Vector3 position, float direction, bool isMain = false)
        {
            var playerID = m_nextPlayerID;
            ++m_nextPlayerID;
            var character = GetItemFromPool();
            character.transform.position = position;
            character.Init(playerID, groupID, info, colourInfo, m_services);
            character.SetDirection(direction);
            character.SetHorizontalMovementLimit(m_xMovementLimit);
            character.SetVerticalMovementLimit(m_yMovementLimit);

            character.SetControlledByPlayer(isMain, m_AICharacterSettings);
            if (isMain)
            {
                m_mainCharacter = character;
            }
            return character;
        }

        public void SetVerticalMovementLimit(Vector2 limit)
        {
            m_yMovementLimit = limit;
            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                m_activeItems[i].SetVerticalMovementLimit(limit);
            }
        }

        public void SetHorizontalMovementLimit(Vector2 limit)
        {
            m_xMovementLimit = limit;
            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                m_activeItems[i].SetHorizontalMovementLimit(limit);
            }
        }

        public void UpdateController(float timeDelta)
        {
            int idx = 0;
            while (idx < m_activeItems.Count)
            {
                if (m_activeItems[idx].ShouldRemoveToPool())
                {
                    ReturnToPool(m_activeItems[idx]);
                }
                else
                {
                    ++idx;
                }
            }

            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                m_activeItems[i].UpdateCharacter(timeDelta);
            }

            if (m_mainCharacter == null || !m_mainCharacter.IsAlive())
            {
                m_services.gameplayLogic.OnLost();
            }
        }

        public List<Character> GetAllCharacters()
        {
            return m_activeItems;
        }

        public bool IsInBattleWithAnyoneElse(Character character, Character attacker)
        {
            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                if (m_activeItems[i] == character || m_activeItems[i] == attacker || !m_activeItems[i].IsAlive())
                    continue;

                if (m_activeItems[i].IsPlayerControlled())
                    continue;

                var otherDistance = character.transform.position - m_activeItems[i].transform.position;
                var thisDistance = character.transform.position - attacker.transform.position;
                if (m_activeItems[i].GetBattleTarget() == character && otherDistance.sqrMagnitude < thisDistance.sqrMagnitude)
                    return true;
            }
            return false;
        }

        public void SetActive(bool value, bool keepAnimations)
        {
            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                if (value || !keepAnimations)
                    m_activeItems[i].SetActive(value);
            }
        }

        public bool IsPathBlocked(Vector3 position, int playerID)
        {
            for (int i = 0; i < m_activeItems.Count; ++i)
            {
                if (m_activeItems[i].GetPlayerID() == playerID || !m_activeItems[i].IsAlive())
                    continue;

                var xMatch = Mathf.Abs(position.x - m_activeItems[i].transform.position.x) < m_services.settings.GetXPathBlock();
                var yMatch = Mathf.Abs(position.y - m_activeItems[i].transform.position.y) < m_services.settings.GetYPathBlock();
                if (xMatch && yMatch)
                    return true;
            }
            return false;
        }
    }
}
