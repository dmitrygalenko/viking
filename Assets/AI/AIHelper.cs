using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

namespace AI
{
    public static class AIHelper
    {
        public static bool IsValidTarget(Character attacker, Character target)
        {
            if (target == null)
                return false;

            if (!target.IsAlive())
                return false;

            return attacker.GetGroupID() != target.GetGroupID();
        }

        public static int GetDirectionIntoCamera(Character character, Services services)
        {
            var limits = services.levelController.GetHorizontalMovementLimit();
            var transform = character.transform;
            var direction = 0;
            if (transform.position.x < limits.x)
            {
                direction = 1;
            }
            else if (transform.position.x > limits.y)
            {
                direction = -1;
            }

            return direction;
        }

        public static bool IsCloseToTarget(Character character, Vector3 target, float minDistance, Vector3 enemy)
        {
            var yFits = Mathf.Abs(target.y - character.transform.position.y) < minDistance;
            if (!yFits)
                return false;

            var posLeft = enemy.x;
            var posRight = target.x;
            if (posLeft > posRight)
            {
                var tmp = posRight;
                posRight = posLeft;
                posLeft = tmp;
            }

            return posLeft <= character.transform.position.x && character.transform.position.x <= posRight;
        }
    }
}
