using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class WalkIntoCameraState : BaseState
    {
        private int m_decidedX;

        public override void Start()
        {
            m_decidedX = 0;
        }

        public override void ProcessPlanned(float deltaTime)
        {
            if (m_decidedX != 0f)
            {
                m_aiCharacter.GetCharacter().Move(m_decidedX, 0f);
            }
        }

        public override void ProcessDecision(float deltaTime)
        {
            m_decidedX = AIHelper.GetDirectionIntoCamera(m_aiCharacter.GetCharacter(), m_services);

            if (m_decidedX == 0)
            {
                m_aiCharacter.ChangeState(FSMState.Idle);
            }
        }

        public override void End()
        {

        }
    }
}
