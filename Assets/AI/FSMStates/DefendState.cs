using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class DefendState : BaseState
    {
        public override void Start()
        {
            m_aiCharacter.GetCharacter().Stop();
        }

        public override void ProcessPlanned(float deltaTime)
        {

        }

        public override void ProcessDecision(float deltaTime)
        {
            var character = m_aiCharacter.GetCharacter();

            var targetCharacter = m_memory.attackTarget;
            if (targetCharacter == null)
            {
                m_aiCharacter.ChangeState(FSMState.Idle);
                return;
            }

            if (!targetCharacter.IsAttacking())
            {
                if (character.IsDefending())
                {
                    character.Defend(false);
                }

                if (AIHelper.IsCloseToTarget(character, m_memory.attackPosition, character.GetRecommendedDistance(), m_memory.attackTarget.transform.position))
                    m_aiCharacter.ChangeState(FSMState.AttackTarget);
                else
                    m_aiCharacter.ChangeState(FSMState.WalkToTarget);
                return;
            }

            var directionToTarget = targetCharacter.transform.position.x - character.transform.position.x;
            if (Mathf.Sign(directionToTarget) != Mathf.Sign(character.GetDirection()))
            {
                // change orientation
                if (character.IsDefending())
                {
                    character.Defend(false);
                    return;
                }

                character.SetDirection(directionToTarget);
            }

            character.Defend(true);
        }

        public override void End()
        {

        }
    }
}
