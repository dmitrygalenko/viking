using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class IdleState : BaseState
    {
        private float m_timeToCry;
        private float m_cryCooldown;

        public override void Start()
        {
            m_aiCharacter.GetCharacter().Move(0f, 0f);
            m_timeToCry = 0f;
            m_cryCooldown = 0f;
        }

        public class WeightedTarget : Helpers.WeightedItem<Gameplay.Character> { }

        public override void ProcessPlanned(float deltaTime)
        {
            if (m_timeToCry > 0f)
            {
                m_timeToCry -= deltaTime;

                if (m_timeToCry <= 0f)
                {
                    m_aiCharacter.GetCharacter().Cry();
                    m_cryCooldown = UnityEngine.Random.Range(0f, m_aiSettings.GetWarCryIdleCooldown());
                }
            }

            if (m_cryCooldown > 0f)
            {
                m_cryCooldown -= deltaTime;
            }
        }

        private float GetTargetWeight(Gameplay.Character character)
        {
            var weight = 0f;
            switch (character.GetGroupID())
            {
                case Gameplay.GroupID.Player:
                    weight = 15f;
                    break;
                case Gameplay.GroupID.Enemy:
                    weight = 10f;
                    break;
            }
            if (weight <= 0f)
                return 0f;

            var distance = m_aiCharacter.GetCharacter().transform.position - character.transform.position;
            weight -= distance.sqrMagnitude * 0.1f;
            return Mathf.Max(0f, weight);
        }

        public override void ProcessDecision(float deltaTime)
        {
            // walk into camera
            if (AIHelper.GetDirectionIntoCamera(m_aiCharacter.GetCharacter(), m_services) != 0)
            {
                m_aiCharacter.ChangeState(FSMState.WalkIntoCamera);
                return;
            }

            // find next target
            if (!AIHelper.IsValidTarget(m_aiCharacter.GetCharacter(), m_memory.attackTarget))
            {
                ChooseNextTarget();
            }

            if (m_memory.attackTarget == null)
                return;

            // if can't battle, shout
            if (m_services.charactersController.IsInBattleWithAnyoneElse(m_memory.attackTarget, m_aiCharacter.GetCharacter()))
            {
                if (m_cryCooldown <= 0f)
                {
                    m_timeToCry = UnityEngine.Random.Range(0f, m_aiSettings.GetTimeToWarCry());
                }
                return;
            }
            else
            {
                m_aiCharacter.ChangeState(FSMState.WalkToTarget);
            }
        }

        private void ChooseNextTarget()
        {
            var characters = m_services.levelController.GetAllCharacters();
            var thisCharacter = m_aiCharacter.GetCharacter();
            var targets = new List<WeightedTarget>(characters.Count);

            for (int i = 0; i < characters.Count; ++i)
            {
                if (!AIHelper.IsValidTarget(thisCharacter, characters[i]))
                    continue;

                targets.Add(new WeightedTarget()
                {
                    item = characters[i],
                    weight = GetTargetWeight(characters[i]),
                });
            }

            var chosenTarget = Helpers.WeightedRandom<Gameplay.Character, WeightedTarget>.GetWeightedItem(targets);
            if (chosenTarget != null)
            {
                m_aiCharacter.ChooseTarget(chosenTarget);
            }
        }

        public override void End()
        {

        }
    }
}
