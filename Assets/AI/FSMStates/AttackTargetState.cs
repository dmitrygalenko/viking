using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AttackTargetState : BaseState
    {
        private float m_attackTimer;

        public override void Start()
        {
            m_attackTimer = 0f;
            m_aiCharacter.GetCharacter().Stop();
        }

        public override void ProcessPlanned(float deltaTime)
        {
            if (m_attackTimer > 0f)
            {
                m_attackTimer -= deltaTime;
                if (m_attackTimer <= 0f)
                {
                    Attack();
                }
            }
        }

        public override void ProcessDecision(float deltaTime)
        {
            var character = m_aiCharacter.GetCharacter();

            if (character.IsOnCooldown())
            {
                m_aiCharacter.ChangeState(FSMState.Idle);
                return;
            }

            var enemy = m_memory.attackTarget;
            if (!AIHelper.IsValidTarget(character, enemy))
            {
                m_aiCharacter.ChangeState(FSMState.Idle);
                return;
            }

            if (!AIHelper.IsCloseToTarget(character, m_memory.attackPosition, character.GetRecommendedDistance(), m_memory.attackTarget.transform.position))
            {
                m_aiCharacter.ChangeState(FSMState.WalkToTarget);
                return;
            }

            m_aiCharacter.UpdateAttackPosition();

            if (m_memory.attackTarget.IsAttacking())
            {
                m_aiCharacter.ChangeState(FSMState.BlockAttack);
                return;
            }

            var distance = enemy.transform.position - character.transform.position;
            character.SetDirection(distance.x);

            if (m_attackTimer > 0f)
                return;

            m_attackTimer = UnityEngine.Random.Range(0f, m_aiSettings.GetAttackTimer());
            if (m_attackTimer <= 0f)
            {
                Attack();
            }
        }

        private void Attack()
        {
            var enemy = m_memory.attackTarget;
            if (!AIHelper.IsValidTarget(m_aiCharacter.GetCharacter(), enemy))
                return;

            // 04 07
            var heavyAttackChance = m_aiSettings.GetOverheadAttackChance(enemy.IsDefending());
            if (m_aiCharacter.GetCharacter().HasRangedWeapon() && m_aiCharacter.GetCharacter().HasAmmo())
                heavyAttackChance = 0f;

            var shouldDoLight = UnityEngine.Random.Range(0f, 1f) > heavyAttackChance;
            if (shouldDoLight)
                m_aiCharacter.GetCharacter().AttackL();
            else
                m_aiCharacter.GetCharacter().AttackH();
        }

        public override void End()
        {

        }
    }
}
