using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class WalkToTargetState : BaseState
    {
        private Vector2 m_decidedDirection;

        public override void Start()
        {
            m_decidedDirection = Vector2.zero;

            if (m_aiCharacter.GetCharacter().IsDefending())
                m_aiCharacter.GetCharacter().Defend(false);
        }

        public override void ProcessPlanned(float deltaTime)
        {
            var character = m_aiCharacter.GetCharacter();
            if (!AIHelper.IsCloseToTarget(m_aiCharacter.GetCharacter(), m_memory.attackPosition, character.GetRecommendedDistance(), m_memory.attackTarget.transform.position))
                m_aiCharacter.GetCharacter().Move(m_decidedDirection.x, m_decidedDirection.y);
        }

        public override void ProcessDecision(float deltaTime)
        {
            var target = m_memory.attackTarget;

            if (target == null)
            {
                m_aiCharacter.ChangeState(FSMState.Idle);
                return;
            }

            if (!target.IsAlive())
            {
                m_aiCharacter.ChangeState(FSMState.Idle);
                return;
            }

            if (m_services.charactersController.IsInBattleWithAnyoneElse(target, m_aiCharacter.GetCharacter()))
            {
                m_aiCharacter.GetCharacter().Stop();
                m_aiCharacter.ChangeState(FSMState.Idle);
                return;
            }

            m_aiCharacter.UpdateAttackPosition();

            var thisCharacter = m_aiCharacter.GetCharacter();
            if (AIHelper.IsCloseToTarget(m_aiCharacter.GetCharacter(), m_memory.attackPosition, thisCharacter.GetRecommendedDistance(), m_memory.attackTarget.transform.position))
            {
                if (target.IsAttacking())
                    m_aiCharacter.ChangeState(FSMState.BlockAttack);
                else
                    m_aiCharacter.ChangeState(FSMState.AttackTarget);
                return;
            }

            var distance = m_memory.attackPosition - thisCharacter.transform.position;

            var distanceToEnemyX = m_memory.attackTarget.transform.position.x - thisCharacter.transform.position.x;
            if (Mathf.Sign(distanceToEnemyX) != Mathf.Sign(distance.x))
            {
                distance.x = 0f;
            }

            thisCharacter.SetDirection(distanceToEnemyX);

            m_decidedDirection = distance.normalized;
        }

        public override void End()
        {

        }
    }
}
