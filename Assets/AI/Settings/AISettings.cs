using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    [CreateAssetMenu(fileName = "AISettings", menuName = "Settings/AISettings")]
    public class AISettings : ScriptableObject
    {
        [SerializeField] private float m_FSMTickTime;
        [SerializeField] private bool m_enableUIDebug;
        [SerializeField] private float m_timeToWarCry;
        [SerializeField] private float m_warCryIdleCooldown;
        [SerializeField] private float m_attackTimer;
        [SerializeField] private float m_overheadAttackChance;
        [SerializeField] private float m_overheadAttackBlockChance;
        [SerializeField] private float m_predictionTime = 1f;

        public float GetFSMTickTime()
        {
            return m_FSMTickTime;
        }

        public bool EnableUIDebug()
        {
            return m_enableUIDebug;
        }

        public float GetTimeToWarCry()
        {
            return m_timeToWarCry;
        }

        public float GetWarCryIdleCooldown()
        {
            return m_warCryIdleCooldown;
        }

        public float GetAttackTimer()
        {
            return m_attackTimer;
        }

        public float GetOverheadAttackChance(bool enemyblocks)
        {
            return enemyblocks ? m_overheadAttackBlockChance : m_overheadAttackChance;
        }

        public float GetPredictionTime()
        {
            return m_predictionTime;
        }
    }
}
