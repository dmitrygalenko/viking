using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public abstract class BaseState
    {
        protected FSMState m_stateId;
        protected AgentMemory m_memory;
        protected AICharacter m_aiCharacter;
        protected Gameplay.Services m_services;
        protected AISettings m_aiSettings;

        public void Init(FSMState state, AICharacter aiCharacter, AgentMemory memory, Gameplay.Services services, AISettings aiSettings)
        {
            m_services = services;
            m_stateId = state;
            m_aiCharacter = aiCharacter;
            m_aiSettings = aiSettings;
            m_memory = memory;
        }

        public FSMState StepType()
        {
            return m_stateId;
        }

        public abstract void Start();
        // happens on each frame
        public abstract void ProcessPlanned(float deltaTime);
        // happens on each AI tick
        public abstract void ProcessDecision(float deltaTime);
        public abstract void End();
    }
}
