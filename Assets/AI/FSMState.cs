using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public enum FSMState
    {
        WalkIntoCamera,
        Idle,
        WalkToTarget,
        AttackTarget,
        EvadeAttack,
        BlockAttack,
        RunFromTarget,
    }
}
