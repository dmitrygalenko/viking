using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AgentMemory
    {
        public Gameplay.Character attackTarget;
        public Vector3 attackPosition;
    }
}
