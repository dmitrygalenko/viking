using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public static class AIStateFactory
    {
        public static BaseState GetState(FSMState state, AICharacter character, AgentMemory memory, Gameplay.Services services, AISettings m_aiSettings)
        {
            BaseState newState = null;
            switch (state)
            {
                case FSMState.WalkIntoCamera:
                    newState = new WalkIntoCameraState();
                    break;
                case FSMState.Idle:
                    newState = new IdleState();
                    break;
                case FSMState.WalkToTarget:
                    newState = new WalkToTargetState();
                    break;
                case FSMState.AttackTarget:
                    newState = new AttackTargetState();
                    break;
                case FSMState.BlockAttack:
                    newState = new DefendState();
                    break;
                case FSMState.EvadeAttack:
                case FSMState.RunFromTarget:
                    break;
            }

            if (newState != null)
            {
                newState.Init(state, character, memory, services, m_aiSettings);
            }
            return newState;
        }
    }
}
