using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

namespace AI
{
    public class AICharacter : MonoBehaviour
    {
        private Character m_controlledCharacter;
        private bool m_active;
        private AgentMemory m_memory;
        private BaseState m_currentState;
        private float m_time;
        private float m_tickTime;
        [Header("Debug")]
        [SerializeField] private GameObject m_VisualDebug;
        [SerializeField] private GameObject m_UIDebug;
        [SerializeField] private GameObject m_targetMark;
        [SerializeField] private UnityEngine.UI.Image m_progress;
        [SerializeField] private TMPro.TextMeshProUGUI m_info;
        private bool m_enableDebugAI;
        private AISettings m_aiSettings;
        private Services m_services;

        public void InitDisabled()
        {
            m_active = false;
        }

        public void Init(Character controlledCharacter, Services services, AISettings AIsettings)
        {
            m_services = services;
            m_controlledCharacter = controlledCharacter;
            m_aiSettings = AIsettings;
            m_memory = new AgentMemory();
            m_active = true;
            m_tickTime = AIsettings.GetFSMTickTime();
            m_time = m_tickTime;
            ChangeState(FSMState.Idle);

            EnableDebugAI(AIsettings.EnableUIDebug());
        }

        private void EnableDebugAI(bool value)
        {
            m_enableDebugAI = value;
            m_VisualDebug.SetActive(value);
            m_UIDebug.SetActive(value);
        }

        public void UpdateAI(float deltaTime)
        {
            if (m_enableDebugAI)
                UpdateDebug();

            if (!m_active || m_currentState == null)
                return;

            var isTickFrame = false;

            if (m_time < m_tickTime)
            {
                m_time += deltaTime;
            }
            else
            {
                while (m_time >= m_tickTime)
                {
                    isTickFrame = true;
                    Tick(m_tickTime);
                    m_time -= m_tickTime;
                }
            }

            if (!isTickFrame)
            {
                m_currentState.ProcessPlanned(deltaTime);
            }
        }

        private void UpdateDebug()
        {
            m_targetMark.transform.position = m_memory.attackPosition;

            if (!m_active)
            {
                m_info.text = "INACTIVE";
                return;
            }

            if (m_currentState == null)
            {
                m_info.text = "N/A";
                return;
            }

            m_progress.fillAmount = Mathf.Clamp01(m_time / m_tickTime);
            m_info.text = m_currentState.StepType().ToString();
        }

        private void Tick(float deltaTime)
        {
            m_currentState.ProcessDecision(deltaTime);
        }

        public Character GetCharacter()
        {
            return m_controlledCharacter;
        }

        public void ChangeState(FSMState state)
        {
            if (m_currentState != null)
            {
                m_currentState.End();
            }

            m_currentState = AIStateFactory.GetState(state, this, m_memory, m_services, m_aiSettings);
            if (m_currentState == null)
            {
                Debug.LogError($"no definition for {state}");
                return;
            }

            m_currentState.Start();
        }

        public Character GetBattleTarget()
        {
            if (m_memory.attackTarget == null)
                return null;

            if (AIHelper.IsCloseToTarget(m_controlledCharacter, m_memory.attackPosition, m_controlledCharacter.GetRecommendedDistance(), m_memory.attackTarget.transform.position))
                return m_memory.attackTarget;

            return null;
        }

        public void ChooseTarget(Character target)
        {
            m_memory.attackTarget = target;
            UpdateAttackPosition();
        }

        public void UpdateAttackPosition()
        {
            if (m_memory.attackTarget == null)
                return;

            var predictionMoved = m_memory.attackTarget.GetCurrentSpeed() * m_aiSettings.GetPredictionTime();
            var attackPlace = m_memory.attackTarget.transform.position + predictionMoved;
            var direction = attackPlace - GetCharacter().transform.position;
            var attackScale = Mathf.Min(m_memory.attackTarget.transform.localScale.x, 1f);
            attackPlace.x += GetCharacter().GetAttackRange() * -Mathf.Sign(direction.x) * attackScale;
            m_memory.attackPosition = attackPlace;
        }
    }
}
